<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Actions;

use Bittacora\Bpanel4\RelatedTags\Traits\SavesRelatedTags;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Bittacora\Content\Content;
use Bittacora\ContentMultimedia\ContentMultimedia;
use Bittacora\Bpanel4\Products\Dtos\ProductCreationDto;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Database\Connection;
use Illuminate\Validation\ValidationException;
use Throwable;

final class CreateProduct
{
    use SavesRelatedTags;

    public function __construct(
        private readonly Connection $db,
        private readonly Content $content,
        private readonly ContentMultimedia $multimedia,
    ) {
    }

    /**
     * @throws ValidationException|Throwable
     */
    public function execute(ProductCreationDto $dto): void
    {
        $this->db->beginTransaction();
        try {
            $product = $this->createProduct($dto);
            $this->multimedia->uploadModelMultimedia($product, $dto->productImages ?? []);
            $this->db->commit();
        } catch (ValidationException $exception) {
            $this->db->rollBack();
            throw $exception;
        }
    }

    private function createProduct(ProductCreationDto $dto): Product
    {
        $vatRate = VatRate::whereId($dto->vatRateId)->firstOrFail();
        $product = new Product();
        $product->setReference($dto->reference);
        $product->setName($dto->name);
        $product->setVatRate($vatRate);
        $product->setPrice($dto->price);
        $product->setStock($dto->stock);
        $product->setShortDescription($dto->shortDescription);
        $product->setDescription($dto->description);
        $product->setSize($dto->size);
        $product->setUnit($dto->unit);
        $product->setDiscountedPrice($dto->discountedPrice);
        $product->setActive($dto->active);
        $product->setFeatured($dto->featured);
        $product->setPosition($dto->position);
        $product->setSlug($dto->slug);
        $product->setMetaTitle($dto->metaTitle != '' ? $dto->metaTitle : $dto->name);
        $product->setMetaDescription($dto->metaDescription != '' ? $dto->metaDescription : $dto->name);
        $product->setMetaKeywords($dto->metaKeywords != '' ? $dto->metaKeywords : $dto->name);
        $product->save();
        $this->saveRelatedTags('condiciones-piel', $product, $dto->skinConditions ?? []);
        $this->saveRelatedTags('protocolos', $product, $dto->protocols ?? []);
        $this->content->associateWithModel($product);
        return $product;
    }
}
