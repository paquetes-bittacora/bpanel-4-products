<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Actions;

use Bittacora\Bpanel4\RelatedTags\Traits\SavesRelatedTags;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Bittacora\ContentMultimedia\ContentMultimedia;
use Bittacora\Bpanel4\Products\Dtos\ProductUpdateDto;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Database\Connection;
use Illuminate\Validation\ValidationException;
use Throwable;

final class UpdateProduct
{
    use SavesRelatedTags;

    public function __construct(
        private readonly Connection $db,
        private readonly ContentMultimedia $multimedia,
    ) {
    }

    /**
     * @throws ValidationException|Throwable
     */
    public function execute(Product $product, ProductUpdateDto $productData): void
    {
        $this->db->beginTransaction();
        try {
            $product = $this->updateProduct($product, $productData);
            $this->multimedia->uploadModelMultimedia($product, $productData->productImages ?? []);
            $this->db->commit();
        } catch (ValidationException $exception) {
            $this->db->rollBack();
            throw $exception;
        }
    }

    /**
     */
    private function updateProduct(Product $product, ProductUpdateDto $dto): Product
    {
        $product->setLocale($dto->locale);
        $product->setReference($dto->reference);
        $product->setName($dto->name);
        $product->setShortDescription($dto->shortDescription);
        $product->setDescription($dto->description);
        $product->setDiscountedPrice($dto->discountedPrice);
        $product->setPrice($dto->price);
        $product->setSize($dto->size);
        $product->setUnit($dto->unit);
        $product->setStock($dto->stock);
        $product->setVatRate(VatRate::whereId($dto->vatRateId)->firstOrFail());
        $product->setActive($dto->active ?? false);
        $product->setFeatured($dto->featured ?? false);
        $product->setPosition($dto->position);
        $product->setSlug($dto->slug);
        $product->setMetaTitle($dto->metaTitle != '' ? $dto->metaTitle : $dto->name);
        $product->setMetaDescription($dto->metaDescription != '' ? $dto->metaDescription : $dto->name);
        $product->setMetaKeywords($dto->metaKeywords != '' ? $dto->metaKeywords : $dto->name);
        $product->save();
        $this->saveTags($product, (array)$dto);
        return $product;
    }

    /**
     * @param array<string, array<string|int>> $productData
     */
    private function saveTags(Product $product, array $productData): void
    {
        $this->saveRelatedTags('condiciones-piel', $product, $productData['skin-conditions'] ?? []);
        $this->saveRelatedTags('protocolos', $product, $productData['protocols'] ?? []);
    }
}
