<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Validation;

use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;

class ProductValidator
{
    public function __construct(private readonly Factory $validationFactory)
    {
    }

    /**
     * @param array<string, string> $data
     * @throws ValidationException
     */
    public function validateProductCreationData(array $data): void
    {
        $this->validationFactory->make($data, $this->getProductCreationValidationFields())->validate();
    }

    /**
     * @param array<string, string> $data
     * @throws ValidationException
     */
    public function validateProductUpdateData(array $data): void
    {
        $this->validationFactory->make($data, $this->getProductUpdateValidationFields())->validate();
    }

    /**
     * @return string[]
     */
    public function getProductCreationValidationFields(): array
    {
        return [
            'reference' => 'string|max:255|required',
            'name' => 'string|max:255|required',
            'short_description' => 'string|max:1000',
            'description' => 'string|max:65000',
            'price' => 'numeric|required|min:0',
            'discounted_price' => 'nullable|numeric|min:0|lt:price',
            'size' => 'nullable|numeric|min:0',
            'unit' => 'nullable|string|max:255|required_with:size',
            'stock' => 'numeric|min:0',
            'product_images.*' => 'nullable|file|max:2000',
            'vat_rate_id' => 'exists:vat_rates,id',
            'active' => 'nullable|boolean',
            'featured' => 'nullable|boolean',
            'skin-conditions' => 'nullable|array',
            'protocols' => 'nullable|array',
            'inci' => 'nullable|string',
            'composition' => 'nullable|string',
            'properties' => 'nullable|string',
            'application' => 'nullable|string',
            'comments' => 'nullable|string',
            'position' => 'numeric|required',
            'slug' => 'string|nullable',
            'meta_title' => 'string|nullable',
            'meta_description' => 'string|nullable',
            'meta_keywords' => 'string|nullable',
        ];
    }

    /**
     * @return string[]
     */
    public function getProductUpdateValidationFields(): array
    {
        return $this->getProductCreationValidationFields();
    }
}
