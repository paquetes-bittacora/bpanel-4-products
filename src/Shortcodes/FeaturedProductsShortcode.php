<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Shortcodes;

use Bittacora\Bpanel4\Products\Models\Product;

final class FeaturedProductsShortcode
{
    public function register($shortcode, $content, $compiler, $name, $viewData): string
    {
        $numberOfProducts = isset($shortcode->toArray()['numberofproducts']) ?
            (int)$shortcode->toArray()['numberofproducts'] :
            8;

        $view = $shortcode->toArray()['view'] ?? 'bpanel4-products::shortcodes.featured-products';

        $products = Product::where('featured', true)->where('active', 1)->inRandomOrder()
            ->limit($numberOfProducts)->get()->all();

        return view($view)->with(['featuredProducts' => $products ?? []])->render();
    }
}