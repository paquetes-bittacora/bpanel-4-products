<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products;

use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Orders\Contracts\CartableProductRepository;
use Bittacora\Bpanel4\Products\Commands\AddMissingTabs;
use Bittacora\Bpanel4\Products\Shortcodes\FeaturedProductsShortcode;
use Bittacora\Bpanel4\RelatedTags\Models\Tag;
use Bittacora\Bpanel4\Products\Commands\FixCategories;
use Bittacora\Bpanel4\Products\Commands\InstallCommand;
use Bittacora\Bpanel4\Products\Http\Livewire\ProductDetails;
use Bittacora\Bpanel4\Products\Http\Livewire\ProductFilters;
use Bittacora\Bpanel4\Products\Http\Livewire\ProductsDatatable;
use Bittacora\Bpanel4\Products\Http\Livewire\ProductsList;
use Bittacora\Bpanel4\Products\Http\Livewire\ProductSmall;
use Bittacora\Bpanel4\Products\Models\CartProduct;
use Bittacora\Bpanel4\Products\Models\Product;
use Bittacora\Bpanel4\Products\Repositories\Bpanel4CartableProductRepository;
use Bittacora\Multimedia\MultimediaFacade;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Livewire;
use Webwizo\Shortcodes\Facades\Shortcode;

final class Bpanel4ProductsServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'bpanel4-products';

    public function boot(): void
    {
        Config::set('orders.product_class', CartProduct::class);
        $this->app->bind(CartableProduct::class, CartProduct::class);
        $this->app->bind(CartableProductRepository::class, Bpanel4CartableProductRepository::class);

        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', self::PACKAGE_PREFIX);
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->commands([
            InstallCommand::class,
            FixCategories::class,
            AddMissingTabs::class,
        ]);
        $this->publishes([__DIR__ . '/../resources/assets' => public_path('assets')], 'product-public-assets');

        $this->registerMediaConversions();
        $this->associateTagsToProducts();
        $this->registerLivewireComponents();
        $this->registerShortcodes();

        $this->publishConfiguration();
    }

    private function registerLivewireComponents(): void
    {
        Livewire::component(
            self::PACKAGE_PREFIX . '::livewire.products-table',
            config('bpanel4-products.datatable_admin_component_class')
        );
        Livewire::component(self::PACKAGE_PREFIX . '::public.livewire.product-filters', ProductFilters::class);
        Livewire::component(self::PACKAGE_PREFIX . '::public.livewire.products-list', ProductsList::class);
        Livewire::component(self::PACKAGE_PREFIX . '::public.livewire.product-details', ProductDetails::class);
        Livewire::component(self::PACKAGE_PREFIX . '::public.product-small', ProductSmall::class);
    }

    private function associateTagsToProducts(): void
    {
        Tag::resolveRelationUsing('products', function ($tagModel) {
            return $tagModel->belongsToMany(Product::class, 'related_taggables', 'tag_id', 'related_taggable_id');
        });
    }

    private function registerMediaConversions(): void
    {
        MultimediaFacade::registerMediaConversion(function ($media) {
            $media->addMediaConversion('product-thumb')
                ->width(config('bpanel4-products.product_thumbnail_width', 400))
                ->height(config('bpanel4-products.product_thumbnail_height', 400))
                ->keepOriginalImageFormat()
                ->performOnCollections('images');
        });
    }

    private function publishConfiguration()
    {
        $this->publishes([
            __DIR__.'/../config/bpanel4-products.php' => config_path('bpanel4-products.php'),
        ], 'config');
    }

    private function registerShortcodes(): void
    {
        if(!str_contains($this->app->make(Request::class)->getRequestUri(), '/bpanel/')) {
            Shortcode::register('featured-products', FeaturedProductsShortcode::class);
        }
    }
}
