<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Events;

use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Http\Request;

final class ProductUpdated
{
    public function __construct(
        private readonly Product $product,
        private readonly Request $request,
    ) {
    }

    public function getProduct(): Product
    {
        return clone $this->product;
    }

    public function getRequest(): Request
    {
        return clone $this->request;
    }
}