<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Contracts;

use Bittacora\Bpanel4\Prices\Types\Price;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\Sluggable\SlugOptions;

interface Product
{
    public function getSlugOptions(): SlugOptions;

    public function resolveRouteBinding($value, $field = null);

    public function getReference(): string;

    public function setReference(string $reference): void;

    public function getName(): string;

    public function setName(string $name): void;

    public function getShortDescription(): ?string;

    public function setShortDescription(?string $shortDescription): void;

    public function getDescription(): ?string;

    public function setDescription(string $description): void;

    public function getSize(): ?float;

    public function setSize(?float $size): void;

    public function getUnit(): ?string;

    public function setUnit(?string $unit): void;

    public function getStock(): ?int;

    public function setStock(?int $stock): void;

    public function getId(): int;

    public function isActive(): bool;

    public function setActive(bool $active): void;

    public function getPrice(): Price;

    public function setPrice(Price $price): void;

    public function getDiscountedPrice(): ?Price;

    public function setDiscountedPrice(?Price $discountedPrice): void;

    public function shippingClasses(): BelongsToMany;

    public function setFeatured(bool $featured): void;

    public function isFeatured(): bool;

}