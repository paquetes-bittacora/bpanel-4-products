<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Contracts;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\View\View;

interface ProductsFilter
{
    public function addToView(View $view): void;

    /**
     * @param array<string, string|string[]|int|int[]> $filters
     */
    public function filterQuery(Builder $query, array $filters): void;
}