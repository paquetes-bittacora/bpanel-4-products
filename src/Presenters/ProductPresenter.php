<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Presenters;

use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Products\Contracts\Product;
use Bittacora\Bpanel4\Vat\Services\VatService;
use Bittacora\Category\Models\CategorizableModel;
use Bittacora\Category\Models\CategoryModel;
use Bittacora\Bpanel4\Products\Services\ProductPriceCalculator;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class ProductPresenter
{
    protected bool $showPricesWithTaxes = false;

    public function __construct(
        protected readonly ProductPriceCalculator $priceCalculator,
        protected readonly Product $product,
        protected readonly VatService $vatService,
    ) {
    }

    public function getProductId(): int
    {
        return $this->product->getId();
    }

    public function getName(): string
    {
        return $this->product->getName();
    }

    public function getShortDescription(): ?string
    {
        return $this->product->getShortDescription();
    }

    public function getDescription(): ?string
    {
        return $this->product->getDescription();
    }

    public function getNameWithSize(): string
    {
        $productName = $this->getName();

        if (null === $this->getUnit() || $this->getUnit() === '') {
            return $productName;
        }

        return $productName . ' (' . $this->getSize() . ' ' . $this->getUnit() . ')';
    }

    /**
     * @return array<Media>
     */
    public function getImages(): array
    {
        return $this->product->getImages();
    }

    /**
     * @return array<Media>
     */
    public function getFeaturedImages(): array
    {
        return $this->product->getFeaturedImages();
    }

    public function getUnit(): ?string
    {
        return $this->product->getUnit();
    }

    public function getSize(): ?float
    {
        return $this->product->getSize();
    }

    /**
     * Precio como string, solo para mostrarlo en vistas. Permite a otros paquetes mostrar rangos de precios, etc.
     * En este módulo, simplemente devuelve el precio como string, se implementa para poder extenderlo.
     */
    public function getDisplayPrice(): string
    {
        if ($this->hasDiscountedPrice()) {
           return $this->getDiscountedPrice()?->toString() . ' <span class="original-price">' .
               $this->getPriceWithoutDiscount()->toString() . '</span>';
        }
        return $this->getPrice()->toString();
    }

    public function getPrice(): Price
    {
        if ($this->showPricesWithTaxes) {
            return Price::fromInt($this->vatService->addVatToPrice(
                $this->priceCalculator->getPrice()->toInt(),
                $this->product->getVatRate())
            );
        }

        return $this->priceCalculator->getPrice();
    }

    public function getPriceWithoutVat(): Price
    {
        return $this->getPrice();
    }

    public function hasDiscountedPrice(): bool
    {
        return $this->getDiscountedPrice() !== null && $this->getPriceWithoutDiscount() !== $this->getDiscountedPrice();
    }

    public function getPriceWithoutDiscount(): Price
    {
        if ($this->showPricesWithTaxes) {
            return Price::fromInt($this->vatService->addVatToPrice(
                $this->priceCalculator->getPriceWithoutDiscount()->toInt(),
                $this->product->getVatRate()
            ));
        }

        return $this->priceCalculator->getPriceWithoutDiscount();
    }

    public function getDiscountedPrice(): ?Price
    {
        if (null === $this->priceCalculator->getDiscountedPrice()) {
            return null;
        }

        if ($this->showPricesWithTaxes) {
            return Price::fromInt($this->vatService->addVatToPrice(
                $this->priceCalculator->getDiscountedPrice()->toInt(),
                $this->product->getVatRate()
            ));
        }
        return $this->priceCalculator->getDiscountedPrice();
    }

    public function getReference(): string
    {
        return $this->priceCalculator->getReference();
    }

    /**
     * @return array<CategoryModel>
     */
    public function getCategories(): array
    {
        $output = [];
        /** @var CategorizableModel[] $categories */
        $categories = $this->product->categorizable();

        foreach ($categories as $category) {
            $output[] = $category->category()->firstOrFail();
        }

        return $output;
    }

    public function getStock(): ?int
    {
        return $this->product->getStock();
    }

    public function showPricesWithTaxes(bool $showPricesWithTaxes): void
    {
        $this->showPricesWithTaxes = $showPricesWithTaxes;
    }

    public function getSlug(): string
    {
        return $this->product->slug;
    }

    public function getCartProductLink(): string
    {
        if (method_exists($this->product, 'getCartProductLink')) {
            return $this->product->getCartProductLink();
        }

        return route('bpanel4-products-public.show', ['product' => $this->product]);
    }
}
