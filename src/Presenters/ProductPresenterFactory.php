<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Presenters;

use Bittacora\Bpanel4\Clients\Services\ClientService;
use Bittacora\Bpanel4\Products\Contracts\Product;
use Bittacora\Bpanel4\Products\Services\ProductPriceCalculator;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Foundation\Application;
use RuntimeException;

final class ProductPresenterFactory
{
    public function __construct(private readonly ClientService $clientService, private readonly Application $app)
    {
    }

    /**
     * @throws BindingResolutionException
     */
    public function make(Product $product): ProductPresenter
    {
        $productData = [
            'product' => $product,
            'priceCalculator' => new ProductPriceCalculator($product, $this->clientService),
        ];

        $productPresenter = $this->app->makeWith(ProductPresenter::class, $productData);

        if (!$productPresenter instanceof ProductPresenter) {
            throw new RuntimeException("No se pudo crear el ProductPresenter");
        }

        return $productPresenter;
    }
}
