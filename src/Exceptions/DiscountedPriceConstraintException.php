<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Exceptions;

use RuntimeException;

final class DiscountedPriceConstraintException extends RuntimeException
{
    /** @var string  */
    protected $message = 'El precio rebajado no puede ser mayor que el precio normal';
}
