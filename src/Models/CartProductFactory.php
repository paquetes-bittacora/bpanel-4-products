<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Models;

use Bittacora\Bpanel4\Clients\Exceptions\UserNotLoggedInException;
use Bittacora\Bpanel4\Clients\Services\ClientService;
use Bittacora\Bpanel4\Products\Presenters\ProductPresenterFactory;
use Bittacora\Bpanel4\Products\Contracts\Product;

final class CartProductFactory
{
    private Product $product;

    public function __construct(
        private readonly ClientService $clientService,
        private readonly ProductPresenterFactory $productPresenterFactory,
    ) {
    }

    public function setProduct(Product $product): self
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @throws UserNotLoggedInException
     */
    public function make(): CartProduct
    {
        $cartProduct = CartProduct::firstOrCreate([
            'product_id' => $this->product->getId(),
        ]);

        $cartProduct->setProductPresenter($this->productPresenterFactory->make($this->product));

        return $cartProduct;
    }
}
