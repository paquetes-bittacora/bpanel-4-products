<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Models;

use App\Traits\Bpanel4ProductTraits;
use Bittacora\Bpanel4\Prices\Casts\PriceCast;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\RelatedTags\Models\Tag;
use Bittacora\Bpanel4\RelatedTags\Traits\HasRelatedTags;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Bittacora\Bpanel4\Vat\Traits\HasVatRate;
use Bittacora\Category\Models\CategorizableModel;
use Bittacora\Category\Traits\HasManyCategorizables;
use Bittacora\Content\Models\ContentModel;
use Bittacora\Content\Traits\RelatesToContentTrait;
use Bittacora\ContentMultimedia\Traits\HasImages;
use Bittacora\Bpanel4\Products\Exceptions\DiscountedPriceConstraintException;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Spatie\Sluggable\HasTranslatableSlug;
use Spatie\Sluggable\SlugOptions;
use Spatie\Translatable\HasTranslations;
use Bittacora\Bpanel4\Shipping\Models\ShippingClass;

/**
 * Bittacora\Bpanel4\Products\Models\Product
 *
 * @property int $id
 * @property string $reference
 * @property string $name
 * @property string|null $short_description
 * @property string|null $description
 * @property string|null $slug
 * @property string|null $metaTitle
 * @property string|null $metaDescription
 * @property string|null $metaKeywords
 * @property float|null $size Tamaño del producto sin la unidad (por ejemplo, volumen)
 * @property string|null $unit Unidad de medida (ml, kg...)
 * @property int|null $stock
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property bool $featured
 * @method static Builder|Product newModelQuery()
 * @method static Builder|Product newQuery()
 * @method static Builder|Product query()
 * @method static Builder|Product whereCreatedAt($value)
 * @method static Builder|Product whereDescrpition($value)
 * @method static Builder|Product whereDiscountedPrice($value)
 * @method static Builder|Product whereId($value)
 * @method static Builder|Product whereName($value)
 * @method static Builder|Product wherePrice($value)
 * @method static Builder|Product whereReference($value)
 * @method static Builder|Product whereShortDescrpition($value)
 * @method static Builder|Product whereSize($value)
 * @method static Builder|Product whereStock($value)
 * @method static Builder|Product whereUnit($value)
 * @method static Builder|Product whereUpdatedAt($value)
 * @mixin Eloquent
 * @property bool $active
 * @property-read CategorizableModel|null $categorizable
 * @property-read ContentModel|null $content
 * @method static Builder|Product whereActive($value)
 * @method static Builder|Product whereDescription($value)
 * @method static Builder|Product whereShortDescription($value)
 * @property int $vat_rate_id
 * @property-read VatRate $vatRate
 * @method static Builder|Product whereVatRateId($value)
 * @property Price $price
 * @property Price|null $discounted_price
 * @property-read int|null $categorizable_count
 * @property-read Collection|Tag[] $protocols
 * @property-read int|null $protocols_count
 * @property-read Collection|Tag[] $skinConditions
 * @property-read int|null $skin_conditions_count
 * @property int $position
 * @method static Builder|Product whereApplication($value)
 * @method static Builder|Product whereComments($value)
 * @method static Builder|Product whereComposition($value)
 * @method static Builder|Product whereInci($value)
 * @method static Builder|Product whereProperties($value)
 */
class Product extends Model implements \Bittacora\Bpanel4\RelatedTags\Contracts\HasRelatedTags, \Bittacora\Bpanel4\Products\Contracts\Product
{
    use RelatesToContentTrait;
    use HasManyCategorizables;
    use HasVatRate;
    use HasRelatedTags;
    use HasImages;
    use SoftDeletes;
    use HasTranslations;
    use Bpanel4ProductTraits;
    use HasTranslatableSlug;

    /**
     * @var string[]
     */
    protected $fillable = [
        'reference',
        'name',
        'short_description',
        'description',
        'price',
        'discounted_price',
        'size',
        'unit',
        'stock',
        'vat_rate_id',
        'active',
    ];

    /**
     * @var array<string, string>
     */
    protected $casts = [
        'id' => 'int',
        'price' => PriceCast::class,
        'discounted_price' => PriceCast::class,
        'size' => 'float',
        'stock' => 'integer',
        'active' => 'boolean',
    ];

    public $translatable = [
        'name',
        'short_description',
        'description',
        'slug',
        'meta_title',
        'meta_description',
        'meta_keywords',
    ];

    protected static function booted(): void
    {
        static::updating(function (Product $product) {
            $product->validateDiscountedPrice($product->price, $product->discounted_price);
        });

        static::creating(function (Product $product) {
            $product->validateDiscountedPrice($product->price, $product->discounted_price);
        });
    }

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug')
            ->doNotGenerateSlugsOnUpdate()
        ;
    }

    public function resolveRouteBinding($value, $field = null)
    {
        if (!is_numeric($value)) {
            return static::findBySlug($value);
        }
        return $this->where('id', $value)->firstOrFail();
    }

    public function getReference(): string
    {
        return $this->reference;
    }

    public function setReference(string $reference): void
    {
        $this->reference = $reference;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getShortDescription(): ?string
    {
        return $this->short_description;
    }

    public function setShortDescription(?string $shortDescription): void
    {
        $this->short_description = $shortDescription;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getSize(): ?float
    {
        return $this->size;
    }

    public function setSize(?float $size): void
    {
        $this->size = $size;
    }

    public function getUnit(): ?string
    {
        return $this->unit;
    }

    public function setUnit(?string $unit): void
    {
        $this->unit = $unit;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(?int $stock): void
    {
        $this->stock = $stock;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function getPrice(): Price
    {
        return $this->price;
    }

    public function setPrice(Price $price): void
    {
        $this->price = $price;
    }

    public function getDiscountedPrice(): ?Price
    {
        return $this->discounted_price;
    }

    public function setDiscountedPrice(?Price $discountedPrice): void
    {
        $this->discounted_price = $discountedPrice;
    }

    /**
     * @return MorphOne<ContentModel>
     */
    public function content(): MorphOne
    {
        return $this->morphOne(ContentModel::class, 'model');
    }

    private function validateDiscountedPrice(Price $regularPrice, ?Price $discountedPrice): void
    {
        if (null === $discountedPrice) {
            return;
        }
        if ($regularPrice->toFloat() > $discountedPrice->toFloat()) {
            return;
        }
        throw new DiscountedPriceConstraintException();
    }

    /**
     * @return BelongsToMany<ShippingClass>
     */
    public function shippingClasses(): BelongsToMany
    {
        return $this->belongsToMany(ShippingClass::class, 'products_shipping_classes', 'product_id');
    }

    public function setFeatured(bool $featured): void
    {
        $this->featured = $featured;
    }

    public function isFeatured(): bool
    {
        return (int)$this->featured === 1;
    }

    /**
     * Este método es necesario para que las relaciones del model base funcionen correctamente cuando se extiende
     * de esta clase.
     */
    public function getMorphClass() {
        return self::class;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): void
    {
        $this->slug = $slug;
    }

    public function getMetaTitle(): ?string
    {
        return $this->meta_title;
    }

    public function setMetaTitle(?string $metaTitle): void
    {
        $this->meta_title = $metaTitle;
    }

    public function getMetaDescription(): ?string
    {
        return $this->meta_description;
    }

    public function setMetaDescription(?string $metaDescription): void
    {
        $this->meta_description = $metaDescription;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->meta_keywords;
    }

    public function setMetaKeywords(?string $metaKeywords): void
    {
        $this->meta_keywords = $metaKeywords;
    }
}
