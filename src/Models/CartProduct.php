<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Models;

use App;
use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Bittacora\Bpanel4\Vat\Services\VatService;
use Bittacora\Bpanel4\Products\Presenters\ProductPresenter;
use Bittacora\Bpanel4\Products\Presenters\ProductPresenterFactory;
use Eloquent;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * Clase que adapta los productos de Bpanel4 al carrito genérico de bPanel4. Es necesaria debido a que los productos
 * tienen distintos tamaños y precios según rol.
 *
 * Se puede entender como un "producto" que incluye además la información sobre el tamaño seleccionado y el tipo de
 * usuario que lo compra, para así poder calcular los distintos precios.
 *
 * IMPORTANTE: NO INSTANCIAR DIRECTAMENTE, USAR CartProductFactory
 *
 * @property int $product_id
 * @property string $client_type
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|CartProduct newModelQuery()
 * @method static Builder|CartProduct newQuery()
 * @method static Builder|CartProduct query()
 * @method static Builder|CartProduct whereClientType($value)
 * @method static Builder|CartProduct whereCreatedAt($value)
 * @method static Builder|CartProduct whereId($value)
 * @method static Builder|CartProduct whereProductId($value)
 * @method static Builder|CartProduct whereUpdatedAt($value)
 * @mixin Eloquent
 */
final class CartProduct extends Model implements CartableProduct
{
    /** @var string  */
    public $table = 'bpanel4_cart_products';

    /** @var string[]  */
    public $guarded = ['id'];

    private ?ProductPresenter $productPresenter = null;

    public function setProductId(int $productId): void
    {
        $this->product_id = $productId;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setClientType(string $clientType): void
    {
        $this->client_type = $clientType;
    }

    public function setProductPresenter(ProductPresenter $productPresenter): void
    {
        $this->productPresenter = $productPresenter;
    }

    /**
     * @return Collection<int, Media>
     */
    public function getImages(): Collection
    {
        return new Collection($this->getProduct()->getImages());
    }

    /**
     * @return Collection<int, Media>
     */
    public function getFeaturedImages(): Collection
    {
        return new Collection($this->getProduct()->getFeaturedImages());
    }

    /**
     * @throws BindingResolutionException
     */
    public function getName(): string
    {
        return $this->getProductPresenter()->getNameWithSize();
    }

    /**
     * @throws BindingResolutionException
     */
    public function getOriginalUnitPrice(): Price
    {
        return $this->getProductPresenter()->getPriceWithoutDiscount();
    }

    /**
     * @throws BindingResolutionException
     */
    public function getDiscountedUnitPrice(): ?Price
    {
        return $this->getProductPresenter()->getDiscountedPrice();
    }

    /**
     * @throws BindingResolutionException
     */
    public function getReference(): string
    {
        return $this->getProductPresenter()->getReference();
    }

    /**
     * @throws BindingResolutionException
     */
    private function getProductPresenter(): ProductPresenter
    {
        if (null === $this->productPresenter) {
            /**
             * @var ProductPresenterFactory $productPresenterFactory
             * @phpstan-ignore-next-line No puedo usar inyección de dependencias
             */
            $productPresenterFactory = App::makeWith(ProductPresenterFactory::class);
            $this->productPresenter = $productPresenterFactory->make($this->getProduct());
        }
        return $this->productPresenter;
    }

    private function getProduct(): Product
    {
        $productClass = config('bpanel4-products.product_class');
        return $productClass::where('id', '=', $this->product_id)->firstOrFail();
    }

    public function getProductId(): int
    {
        return $this->product_id;
    }

    public function getVatRate(): ?VatRate
    {
        return $this->getProduct()->getVatRate();
    }

    public function getQuantity(): int
    {
        /** @phpstan-ignore-next-line */
        return $this->pivot->quantity;
    }

    public function getCartableProductId(): int
    {
        return $this->pivot->id;
    }

    /**
     * @throws BindingResolutionException
     */
    public function getUnitPrice(): Price
    {
        if ($this->productHasDiscountedPrice() && null !== $this->getDiscountedUnitPrice()) {
            return $this->getDiscountedUnitPrice();
        }
        return $this->getOriginalUnitPrice();
    }

    public function getUnitPriceWithoutVat(): Price
    {
        return $this->discountTaxes($this->getUnitPrice());
    }

    public function getOriginalUnitPriceWithoutVat(): Price
    {
        return $this->discountTaxes($this->getOriginalUnitPrice());
    }

    private function discountTaxes(?Price $price): ?Price
    {
        if (null === $price) {
            return null;
        }

        /** @var VatService $vatService */
        $vatService = resolve(VatService::class);

        return Price::fromInt($vatService->discountVatFromPrice($price->toInt(), $this->getVatRate()));
    }

    /**
     * @throws BindingResolutionException
     */
    private function productHasDiscountedPrice(): bool
    {
        return null !== $this->getDiscountedUnitPrice() &&
            ($this->getDiscountedUnitPrice() < $this->getOriginalUnitPrice());
    }

    /**
     * @throws BindingResolutionException
     * @throws InvalidPriceException
     */
    public function getTotal(): Price
    {
        return new Price($this->getUnitPrice()->toFloat() * $this->getQuantity());
    }

    public function getTotalWithoutVat(): Price
    {
        return $this->discountTaxes($this->getTotal());
    }

    public function getShippingClasses(): Collection
    {
        return $this->getProduct()->shippingClasses()->get();
    }

    public function getStock(): int
    {
        return $this->getProduct()->getStock();
    }

    /**
     * @throws BindingResolutionException
     */
    public function getCartProductLink(): string
    {
        return $this->getProductPresenter()->getCartProductLink();
    }
}
