<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Services;

use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Clients\Services\ClientService;
use Bittacora\Bpanel4\Products\Models\Product;

class ProductPriceCalculator
{
    public function __construct(
        private readonly Product $product,
        private readonly ClientService $clientService,
    ) {
    }

    public function getPriceWithoutDiscount(): Price
    {
        return $this->product->getPrice();
    }

    public function getReference(): string
    {
        return $this->product->getReference();
    }

    public function getPrice(): Price
    {
        return $this->getDiscountedPrice() ?? $this->product->getPrice();
    }

    public function getDiscountedPrice(): ?Price
    {
        return $this->product->getDiscountedPrice();
    }

}
