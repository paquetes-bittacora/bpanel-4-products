<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Dtos;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;

trait PrepareRequestPricesTrait
{
    /**
     * @param array{
     *      price: string,
     *      discounted_price: string
     * } $requestData
     * @return array<string, Price|string|array<string,Price>>
     * @throws InvalidPriceException
     */
    private static function prepareRequestPrices(array $requestData): array
    {
        $requestData['price'] = new Price((float)$requestData['price']);
        $requestData['discounted_price'] = null !== $requestData['discounted_price'] ?
            new Price((float)$requestData['discounted_price']) : null;

        return $requestData;
    }
}
