<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Dtos;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Support\Dtos\Dto;

/**
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 * @SuppressWarnings(PHPMD.ExcessiveParameterList)
 */
final class ProductCreationDto implements Dto
{
    use PrepareRequestPricesTrait;

    /**
     * @param array<string, string>|null $productImages
     * @param array<int>|null $skinConditions
     * @param array<int>|null $protocols
     */
    public function __construct(
        public readonly string $reference,
        public readonly string $name,
        public readonly int $vatRateId,
        public readonly Price $price,
        public readonly ?Price $discountedPrice = null,
        public readonly int $stock = 0,
        public readonly string $shortDescription = '',
        public readonly string $description = '',
        public readonly ?float $size = null,
        public readonly ?string $unit = null,
        public readonly ?array $productImages = null,
        public readonly ?array $skinConditions = null,
        public readonly ?array $protocols = null,
        public readonly bool $active = true,
        public readonly bool $featured = false,
        public readonly int $position = 0,
        public readonly ?string $slug = null,
        public readonly ?string $metaTitle = null,
        public readonly ?string $metaDescription = null,
        public readonly ?string $metaKeywords = null,
    ) {
    }

    /**
     * @throws InvalidPriceException
     */
    public static function map(array $data): Dto
    {
        $data = self::prepareRequestPrices($data);
        return new self(
            reference: $data['reference'],
            name: $data['name'],
            vatRateId: (int) $data['vat_rate_id'],
            price: $data['price'],
            discountedPrice: $data['discounted_price'],
            stock: (int) ($data['stock'] ?? 0),
            shortDescription: $data['short_description'],
            description: $data['description'],
            size: is_numeric($data['size']) ?  (float) $data['size'] : null,
            unit: $data['unit'],
            productImages: $data['product_images'] ?? [],
            skinConditions: $data['skin-conditions'] ?? [],
            protocols: $data['protocols'] ?? [],
            active: isset($data['active']) ? $data['active'] == 1 : false,
            featured: isset($data['featured']) ? $data['featured'] == 1 : false,
            position: (int) $data['position'],
            slug: $data['slug'] ?? null,
            metaTitle: $data['meta_title'] ?? null,
            metaDescription: $data['meta_description'] ?? null,
            metaKeywords: $data['meta_keywords'] ?? null,
        );
    }
}
