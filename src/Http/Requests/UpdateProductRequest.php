<?php

namespace Bittacora\Bpanel4\Products\Http\Requests;

use Bittacora\Bpanel4\Products\Validation\ProductValidator;
use Illuminate\Foundation\Http\FormRequest;

final class UpdateProductRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return string[]
     */
    public function rules(ProductValidator $productValidator): array
    {
        return $productValidator->getProductUpdateValidationFields() + ['id' => 'required', 'locale' => 'required'];
    }
}
