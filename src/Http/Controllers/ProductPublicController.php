<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Products\Presenters\ProductPresenterFactory;
use Bittacora\Category\CategoryFacade;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Bittacora\Category\Models\CategoryModel;

final class ProductPublicController extends Controller
{
    public function __construct(private readonly Factory $view)
    {
    }

    public function index(Request $request): View
    {
        return $this->view->make(
            'bpanel4-products::public.catalog',
            ['selectedCategoryName' => $this->getDisplayedCategoryName($request)]
        );
    }

    public function show(Product $product, ProductPresenterFactory $productPresenterFactory): View
    {
        return $this->view->make('bpanel4-products::public.show', [
            'product' => $product,
            'productPresenter' => $productPresenterFactory->make($product),
            'availableCategories' => CategoryFacade::getCategoriesFromModule(Product::class,),
        ]);
    }

    protected function getDisplayedCategoryName(Request $request): string
    {
        if (!empty($request->query('categories'))) {
            $categoryId = $request->query('categories')[0];
            return CategoryModel::whereId((int) $categoryId)->first()?->title ?? 'Catálogo';
        }

        return 'Catálogo';
    }
}
