<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Products\Events\ProductCreated;
use Bittacora\Bpanel4\Products\Events\ProductUpdated;
use Bittacora\Category\Category;
use Bittacora\Category\Models\CategorizableModel;
use Bittacora\Category\Traits\CategoryController;
use Bittacora\Bpanel4\Products\Actions\CreateProduct;
use Bittacora\Bpanel4\Products\Actions\UpdateProduct;
use Bittacora\Bpanel4\Products\Dtos\ProductCreationDto;
use Bittacora\Bpanel4\Products\Dtos\ProductUpdateDto;
use Bittacora\Bpanel4\Products\Http\Requests\CreateProductRequest;
use Bittacora\Bpanel4\Products\Http\Requests\UpdateProductRequest;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Validation\ValidationException;
use Illuminate\View\Factory;
use Throwable;

class ProductAdminController extends Controller
{
    use CategoryController;

    protected ?ProductAdminController $decorated = null;

    public function __construct(
        protected readonly Factory $view,
        protected readonly UrlGenerator $route,
        protected readonly Redirector $redirector,
        protected readonly Category $categoryModule,
        protected readonly Dispatcher $eventsDispatcher,
    ) {
        $this->setCategoryControllerProperties(Product::class);
    }

    public function setDecorated(ProductAdminController $productAdminController): void
    {
        $this->decorated = $productAdminController;
    }

    public function index(): View
    {
        $this->authorize('bpanel4-products.index');
        $lowStockProducts = Product::where('stock', '<=', 10)->get();
        return $this->view->make('bpanel4-products::bpanel.index', [
            'lowStockProducts' => $lowStockProducts,
        ]);
    }

    public function create(?View $view = null): View
    {
        $this->authorize('bpanel4-products.create');
        $view = $this->view->make('bpanel4-products::bpanel.create')
            ->with([
                'product' => null,
                'action' => $this->route->route('bpanel4-products.store'),
                'categories' => $this->categoryModule->getAllAsArray(Product::class),
            ]);

        $this->decorated?->create($view);

        return $view;
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function store(
        CreateProductRequest $request,
        CreateProduct $createProduct,
    ): RedirectResponse {
        $this->authorize('bpanel4-products.store');

        $dto = ProductCreationDto::map($request->all());
        $createProduct->execute($dto);

        $product = Product::where(['reference' => $request->validated('reference')])->firstOrFail();

        $this->categoryModule->associateToModel($product, $request->input('category'));

        $this->eventsDispatcher->dispatch(new ProductCreated($product, $request));

        return $this->redirector->route('bpanel4-products.index')
            ->with(['alert-success' => __('bpanel4-products::product.created')]);
    }

    public function edit(Product $model, string $locale = 'es', ?View $view = null): View
    {
        $this->authorize('bpanel4-products.edit');
        $model->setLocale($locale);

        $view = $this->view->make('bpanel4-products::bpanel.edit')
            ->with([
                'product' => $model,
                'action' => $this->route->route('bpanel4-products.update', $model),
                'categories' => $this->categoryModule->getAllAsArray(Product::class),
                'language' => $locale,
            ]);

        $this->decorated?->edit($model, $locale, $view);

        return $view;
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function update(
        UpdateProductRequest $request,
        UpdateProduct $updateProduct,
        Product $product
    ): RedirectResponse {
        $this->authorize('bpanel4-products.update');

        $dto = ProductUpdateDto::map($request->validated());
        $updateProduct->execute($product, $dto);

        $this->categoryModule->associateToModel($product, $request->input('category'));

        $this->eventsDispatcher->dispatch(new ProductUpdated($product, $request));

        return $this->redirector->route('bpanel4-products.index')
            ->with(['alert-success' => __('bpanel4-products::product.updated')]);
    }

    public function destroy(Product $product): RedirectResponse
    {
        $this->authorize('bpanel4-products.destroy');

        $this->categoryModule->deleteModelCategorizable($product);
        if (!is_null($product->categorizable) && $product->categorizable->count() > 0) {
            $idsCategorizable = CategorizableModel::where('category_id', $product->categorizable->category->getId())
                ->ordered()->pluck('categorizable_id');
            CategorizableModel::setNewOrder($idsCategorizable, 1, 'categorizable_id');
        }
        $product->delete();
        return $this->redirector->route('bpanel4-products.index')
            ->with(['alert-success' => __('bpanel4-products::product.deleted')]);
    }

    /**
     * Este método se usa en las clases que decoran a este controlador para comprobar que los tipos son correctos. No
     * es obligatorio llamarla ni tiene ningún objetivo más que comprobar tipos.
     * @phpstan-assert View $view
     */
    protected function checkDecoratorValidity(?View $view): void
    {
        if (null === $view) {
            throw new \RuntimeException('View sólo puede ser null en el controlador raíz.');
        }
    }
}
