<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Http\Livewire;

use Bittacora\Bpanel4\Products\Models\Product;
use Bittacora\Bpanel4\Products\Presenters\ProductPresenter;
use Bittacora\Bpanel4\Products\Services\ProductPriceCalculator;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

final class ProductDetails extends ProductLivewireComponent
{
    public const ALLOWED_TO_REPLACE_FIELDS = ['description', 'short_description'];
    public ?int $productId = null;

    private Product $product;

    /**
     * @var Media[]
     */
    public array $productImages = [];
    public array $replacedProductImages = [];
    private ProductPresenter $productPresenter;

    /**
     * @var string[]
     */
    public $listeners = [
        'changeQuantity' => 'updateQuantity',
        // Con este listener permitimos a los plugins modificar distintos contenidos de la ficha del producto
        'replaceProductDetails' => 'replaceProductDetails',
    ];

    public string $featuredImageUrl = '';

    /** @var array<string, string|Media[]> */
    public array $replacedValues = [];

    public function boot(): void
    {
        parent::boot();
    }

    /**
     * @throws BindingResolutionException
     */
    public function booted(): void
    {
        /** @var class-string<Product> $productClass */
        $productClass = config('bpanel4-products.product_class');

        $this->product = $productClass::where('id', $this->productId)->firstOrFail();

        $productData = ['product' => $this->product];

        $this->productPresenter = App::makeWith(
            ProductPresenter::class,
            $productData + [
                'priceCalculator' => App::makeWith(ProductPriceCalculator::class, $productData),
            ]
        );
        $this->productPresenter->showPricesWithTaxes(config('bpanel4-products.show_prices_with_taxes'));

        $this->setDefaultFeaturedImage();
        $this->productImages = $this->productPresenter->getImages();
    }

    public function render(): View
    {
        $this->changeReplacedValues();

        return view('bpanel4-products::public.livewire.product-details', [
            'product' => $this->product,
            'productPresenter' => $this->productPresenter,
        ]);
    }

    private function disallowNegativeQuantities(): void
    {
        if ($this->quantity <= 0) {
            $this->quantity = 1;
        }
    }

    public function setFeaturedImageUrl(string $imageUrl): void
    {
        $this->featuredImageUrl = $imageUrl;
        $this->emit('featuredImageUrlChanged');
    }

    public function replaceProductDetails(array $newProductDetails): void
    {
        foreach ($newProductDetails as $field => $value) {
            $this->replaceFeaturedImage($field, $value);
            $this->replaceRegularImages($field, $value);

            if (!in_array($field, self::ALLOWED_TO_REPLACE_FIELDS, true)) {
                unset($newProductDetails[$field]);
            }
        }

        $this->replacedValues = $newProductDetails;
    }

    private function setDefaultFeaturedImage(): void
    {
        if ($this->productPresenter->getFeaturedImages()) {
            $this->featuredImageUrl = $this->productPresenter->getFeaturedImages()[0]->getFullUrl();
        }
    }

    private function changeReplacedValues(): void
    {
        foreach ($this->replacedValues as $field => $value) {
            $this->product->$field = $value;
        }

        if ([] !== $this->replacedProductImages) {
            $this->productImages = $this->prepareProductImages($this->replacedProductImages);
        }
    }

    /**
     * @param array{id: int|string} $images
     * @return Media[]
     */
    private function prepareProductImages(array $images): array
    {
        $output = [];

        foreach ($images as $image) {
            if(is_array($image)) {
                $output[] = Media::where('id', $image['id'])->firstOrFail();
                continue;
            }
            $output[] = $image;
        }

        return $output;
    }

    private function replaceFeaturedImage(int|string $field, mixed $value): void
    {
        if ('featured_image_url' === $field) {
            $this->setFeaturedImageUrl($value ?? '');
        }
    }

    private function replaceRegularImages(int|string $field, mixed $value): void
    {
        if ('images' === $field) {
            $images = $this->prepareProductImages($value);
            $this->replacedProductImages = $images;
        }
    }
}
