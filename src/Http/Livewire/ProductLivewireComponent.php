<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Http\Livewire;

use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Bittacora\Bpanel4\Clients\Exceptions\UserNotLoggedInException;
use Bittacora\Bpanel4\Orders\Actions\Cart\AddProductToCart;
use Bittacora\Bpanel4\Products\Contracts\Product;
use Bittacora\Bpanel4\Products\Models\CartProduct;
use Bittacora\Bpanel4\Products\Models\CartProductFactory;
use Bittacora\Bpanel4\Products\Presenters\ProductPresenterFactory;
use Illuminate\Support\Facades\App;
use Livewire\Component;

abstract class ProductLivewireComponent extends Component
{
    /**
     * @var int
     */
    public $quantity = 1;

    protected string $productClass;

    public function boot(): void
    {
        $this->productClass = config('bpanel4-products.product_class');
    }

    /**
     * @throws UserNotLoggedInException
     */
    public function addToCart(
        ClientService $clientService,
        int $productId,
        AddProductToCart $addProductToCart
    ): void {
        $product = $this->productClass::where('id', '=', $productId)->firstOrFail();
        $cartProduct = $this->getCartProduct($product, $clientService);
        $addProductToCart->execute($clientService->getClientCart(), $cartProduct, (int) $this->quantity);
        $this->emit('cartUpdated');
        $this->emit('productAdded');
    }

    /**
     * @throws UserNotLoggedInException
     */
    private function getCartProduct(
        Product $product,
        ClientService $clientService
    ): CartProduct {
        $cartProductFactory = new CartProductFactory($clientService, App::make(ProductPresenterFactory::class));
        $cartProductFactory->setProduct($product);
        return $cartProductFactory->make();
    }

    /**
     * Evitamos que el componente falle si el usuario deja vacío el campo de cantidad
     */
    public function updatingQuantity(mixed &$newQuantity): void
    {
        if (!is_numeric($newQuantity) || $newQuantity < 1) {
            $newQuantity = 1;
            $this->quantity = 1;
        }
    }
}
