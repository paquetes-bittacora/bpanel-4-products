<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Http\Livewire;

use Bittacora\Bpanel4\Clients\Exceptions\UserNotLoggedInException;
use Bittacora\Bpanel4\Clients\Services\ClientService;
use Bittacora\Bpanel4\Products\Contracts\Product;
use Bittacora\Bpanel4\Products\Models\CartProduct;
use Bittacora\Bpanel4\Products\Models\CartProductFactory;
use Bittacora\Bpanel4\Products\Presenters\ProductPresenter;
use Bittacora\Bpanel4\Products\Presenters\ProductPresenterFactory;
use Bittacora\Bpanel4\Products\Services\ProductPriceCalculator;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

final class ProductSmall extends ProductLivewireComponent
{
    public int $productId;

    /**
     * @var array<string, string|int|array<Media>>
     */
    public array $productData = []; // No paso el modelo a la vista porque se podrían ver todos los precios en el json

    protected string $productClass;

    private ProductPresenter $productPresenter;

    public function mount(): void
    {
        $this->productClass = config('bpanel4-products.product_class');
    }

    /**
     * @throws BindingResolutionException
     */
    public function render(): View
    {
        $this->initialize();

        return view('bpanel4-products::public.livewire.product-small', [
            'productPresenter' => $this->productPresenter,
        ]);
    }


    /**
     * @throws BindingResolutionException
     */
    private function initialize(): void
    {
        $product = $this->productClass::where('id', '=', $this->productId)->firstOrFail();
        $productData = ['product' => $product];
        $this->productPresenter = App::makeWith(
            ProductPresenter::class,
            $productData + [
                'priceCalculator' => App::makeWith(ProductPriceCalculator::class, $productData),
            ]
        );
        $this->productPresenter->showPricesWithTaxes(config('bpanel4-products.show_prices_with_taxes'));
    }

    /**
     * @throws UserNotLoggedInException
     */
    private function getCartProduct(
        Product $product,
        ClientService $clientService
    ): CartProduct {
        $cartProductFactory = new CartProductFactory($clientService, App::make(ProductPresenterFactory::class));
        $cartProductFactory->setProduct($product);
        return $cartProductFactory->make();
    }
}
