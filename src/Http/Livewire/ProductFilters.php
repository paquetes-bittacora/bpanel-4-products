<?php

/** @noinspection PhpMissingFieldTypeInspection */

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Http\Livewire;

use Bittacora\Bpanel4\Products\Contracts\ProductsFilter;
use Bittacora\Category\CategoryFacade;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class ProductFilters extends Component
{
    /** @var string[] $queryString */
    public $queryString = ['minPrice', 'maxPrice', 'categories', 'name', 'additionalFilters'];

    // No uso tipos porque si no, si el input no tiene texto se considera string y falla.
    /** @var string $name */
    public $name = '';
    /** @var int $minPrice */
    public $minPrice = 0;
    /** @var int $maxPrice */
    public $maxPrice = 0;

    /** @var array<int|string> */
    public $categories = [];

    public $additionalFilters = [];

    public $listeners = ['updateFilters' => 'updateFilters'];

    public function render(): View
    {
        $view = view('bpanel4-products::public.livewire.product-filters', [
            'availableCategories' => CategoryFacade::getCategoriesFromModule(Product::class, true),
            'additionalFilters' => $this->additionalFilters,
        ]);

        /** @var ProductsFilter $filter */
        foreach (app()->tagged('products-filter') as $filter) {
            $filter->addToView($view);
        }

        return $view;
    }

    public function toggleCategory(string $categoryId): void
    {
        $this->categories = [(int) $categoryId];
        $this->updateProductsList();
    }

    public function clearCategoriesFilter(): void
    {
        $this->categories = [];
        $this->updateProductsList();
    }

    public function updateFilters(): void
    {
        $this->updateProductsList();
    }

    /**
     * @return array<string, int|string>
     */
    private function getFilters(): array
    {
        return [
            'name' => $this->name,
            'minPrice' => (int)$this->minPrice,
            'maxPrice' => (int)$this->maxPrice,
            'categories' => $this->categories,
            'additionalFilters' => $this->additionalFilters,
        ];
    }

    private function updateProductsList(): void
    {
        $this->emit('filtersUpdated', $this->getFilters());
    }

    protected function updatedAdditionalFilters(): void
    {
        $this->updateFilters();
    }
}
