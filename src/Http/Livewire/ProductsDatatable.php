<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Http\Livewire;

use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class ProductsDatatable extends DataTableComponent
{
    private string $productClass;

    /**
     * @return array<Column>
     */
    public function columns(): array
    {
        return [
            Column::make('Nombre', 'name')->searchable(fn(Builder $query, $searchTerm) => $query
                ->whereRaw('LOWER(`name`) LIKE ?', [addslashes(strtolower('%' . trim(json_encode($searchTerm), '"'))) . '%'])
            ),
            Column::make('Referencia', 'reference'),
            Column::make('Tamaño', 'size')->format(fn($value, $row, Column $column) => $row->size . ' ' . $row->unit),
            Column::make('Precio', 'price')->format(fn($value, $row, Column $column) => $row->getPrice()),
            Column::make('Precio rebajado', 'discounted_price')->format(fn($value, $row, Column $column) => $row->getDiscountedPrice() ?? ''),
            Column::make('Fecha de creación', 'created_at')->format(fn($value, $row, Column $column) => $row->created_at->format('d/m/Y')),
            Column::make('Activo', 'active')->view('bpanel4-products::bpanel.livewire.datatable-columns.active'),
            Column::make('Destacado', 'featured')->view('bpanel4-products::bpanel.livewire.datatable-columns.featured'),
            Column::make('Acciones', 'id')->view('bpanel4-products::bpanel.livewire.datatable-columns.actions'),
        ];
    }

    /**
     * @return Builder<Product>
     */
    public function query(): Builder
    {
        /** @var Builder<Product> */
        return $this->productClass::query()
            ->select('unit', 'size')
            ->orderBy('created_at', 'DESC')
            ->when(
                $this->getAppliedFilterWithValue('search'),
                fn ($query, $term) => $query
                ->where('name', 'like', '%' . $term . '%')
                ->orWhere('reference', 'like', '%' . $term . '%')
            );
    }

    public function rowView(): string
    {
        return 'bpanel4-products::bpanel.livewire.product-datatable';
    }

    /**
     * @return string[]
     */
    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar',
        ];
    }

    public function bulkDelete(): void
    {
        if ([] !== $this->selected) {
            $this->productClass::destroy($this->selected);
        }
    }

    public function setTableRowClass(Product $row): string
    {
        return 'rappasoft-centered-row';
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
        $this->productClass = config('bpanel4-products.product_class');
    }

    public function builder(): Builder
    {
        return $this->query();
    }
}
