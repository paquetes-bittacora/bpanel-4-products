<?php

/** @noinspection PhpUnused */
/** @noinspection PhpMissingFieldTypeInspection */

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Http\Livewire;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Products\Contracts\ProductsFilter;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithPagination;
use Bittacora\Category\Models\CategoryModel;

class ProductsList extends Component
{
    use WithPagination;

    /** @var array<string, int|string|array<int|string>> $filters */
    private $filters = [];

    public $listeners = ['filtersUpdated', 'filtersUpdated'];

    public $limit = null;

    public int $totalResults = 0; // Se usa en los tests

    public string $selectedCategoryName = 'Catálogo';

    protected $paginationTheme = 'bootstrap';

    protected Application $app;

    /**
     * @var mixed|true
     */
    public bool $filtersHaveBeenSet = false;

    public ?int $categoryId = null;

    public function boot(Application $app): void
    {
        $this->app = $app;
    }

    /**
     * @return Builder<Product>
     * @throws InvalidPriceException
     */
    public function buildQuery(): Builder
    {
        if (!$this->filtersHaveBeenSet && null === $this->categoryId) {
            return Product::where('id', '=', -1);
        }

        $query = Product::where('active', '=', 1);

        $this->filterByText($query);
        $this->filterByMinPrice($query);
        $this->filterByMaxPrice($query);
        $this->filterByCategories($query);
        $this->filterByTagType($query, 'skin-conditions');
        $this->filterByTagType($query, 'protocols');
        $this->filterByAdditionalFilters($query);

        if (null !== $this->limit) {
            $query->limit($this->limit);
        }

        if (null !== $this->categoryId) {
            $query->whereHas('categorizable', function ($query) {
                $query->where('category_id', $this->categoryId);
            });
        }

        $query->orderBy('position', 'ASC')->orderBy('name->' . App::getLocale(), 'ASC');

        return $query;
    }

    public function render(): View
    {
        $this->setDisplayedCategoryName();
        // Esto no debe hacerse, pero no he sido capaz de testear este componente de otra forma
        if (App::runningUnitTests()) {
            $this->totalResults = $this->buildQuery()->count();
        }

        $query = $this->buildQuery();

        if (null !== $this->limit) {
            $query = $query->take($this->limit)->get();
        } else {
            $query = $this->buildQuery()->paginate(config('bpanel4-products.products_per_page', 16));
        }


        return view('bpanel4-products::public.livewire.products-list', [
            'products' => $query,
        ]);
    }

    /**
     * @param array<string, int|string|array<int|string>> $filters
     */
    public function filtersUpdated(array $filters): void
    {
        $this->filtersHaveBeenSet = true;
        $this->filters = $filters;
        $this->resetPage();
    }

    /**
     * @param Builder<Product> $query
     */
    private function filterByText(Builder $query): void
    {
        if (!isset($this->filters['name']) || strlen($this->filters['name']) < 3) {
            return;
        }

        $text = $this->filters['name'];

        $query->where(static function (Builder $query) use ($text) {
            $query->orWhereRaw('UPPER(`name`) LIKE ?', [mb_strtoupper('%' . trim($text) . '%')]);
            $query->orWhereRaw('LOWER(`name`) LIKE ?', [mb_strtolower('%' . trim($text) . '%')]);
            $query->orWhereRaw('LOWER(`name`) LIKE ?', [addslashes(mb_strtolower('%' . trim(json_encode($text), '"'))) . '%']);
            $query->orWhereRaw('UPPER(`name`) LIKE ?', [addslashes(mb_strtoupper('%' . trim(json_encode($text), '"'))) . '%']);
            $query->orWhere('description', 'LIKE', '%' . $text . '%');
        });
    }

    /**
     * @param Builder<Product> $query
     * @throws InvalidPriceException
     */
    private function filterByMinPrice(Builder $query): void
    {
        if (!empty($this->filters['minPrice'])) {
            $minPrice = (new Price((int)$this->filters['minPrice']))->toInt();
            $query->where(function (Builder $query) use ($minPrice) {
                $query->whereNull($this->getPriceField(true))->where(
                    $this->getPriceField(),
                    '>=',
                    $minPrice,
                );
            })->orWhere(function (Builder $query) use ($minPrice) {
                $query->whereNotNull($this->getPriceField(true))->where(
                    $this->getPriceField(true),
                    '>=',
                    $minPrice,
                );
            });
        }
    }

    /**
     * @param Builder<Product> $query
     * @throws InvalidPriceException
     */
    private function filterByMaxPrice(Builder $query): void
    {
        if (!empty($this->filters['maxPrice'])) {
            $maxPrice = (new Price((int)$this->filters['maxPrice']))->toInt();
            $query->where(
                $this->getPriceField(),
                '<=',
                $maxPrice,
            )->orWhere(function (Builder $query) use ($maxPrice) {
                $query->whereNotNull($this->getPriceField(true))->where(
                    $this->getPriceField(true),
                    '<=',
                    $maxPrice,
                );
            });
        }
    }

    /**
     * @param Builder<Product> $query
     */
    private function filterByCategories(Builder $query): void
    {
        if (empty($this->filters['categories'])) {
            return;
        }

        $categories = $this->filters['categories'];
        $query->whereHas('categorizable', function ($query) use ($categories) {
            $query->whereIn('category_id', $categories);
        });
    }

    /**
     * @param Builder<Product> $query
     */
    private function filterByTagType(Builder $query, string $typeName): void
    {
        if (!isset($this->filters[$typeName]) || empty($this->filters[$typeName])) {
            return;
        }

        $tags = $this->filters[$typeName];
        $query->whereHas(Str::camel($typeName), function ($query) use ($tags) {
            $query->whereIn('tag_id', $tags);
        });
    }

    private function getPriceField(bool $discounted = false): string
    {
        $discountString = $discounted ? 'discounted_' : '';

        return $discountString . 'price';
    }

    private function filterByAdditionalFilters(Builder $query): void
    {
        /** @var ProductsFilter $filter */
        foreach (app()->tagged('products-filter') as $filter) {
            $filter->filterQuery($query, $this->filters);
        }
    }

    public function dehydrate(): void
    {
        $this->emit('productListUpdated');
        $this->emit('categoryNameUpdated', $this->selectedCategoryName);
    }

    protected function setDisplayedCategoryName(): void
    {
        if (!empty($this->filters['categories'])) {
            $categoryId = $this->filters['categories'][0];
            $category = CategoryModel::whereId((int) $categoryId)->first()?->title ?? 'Catálogo';
            $this->selectedCategoryName = $category;
        }
    }
}
