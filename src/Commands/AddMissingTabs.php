<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Commands;

use App\Traits\Bpanel4ProductTraits;
use Artisan;
use Bittacora\AdminMenu\AdminMenu;
use Bittacora\AdminMenu\AdminMenuFacade;
use Bittacora\Bpanel4\Products\Bpanel4ProductsServiceProvider;
use Bittacora\Bpanel4\Products\Models\Product;
use Bittacora\Category\CategoryFacade;
use Bittacora\Tabs\Tabs;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class AddMissingTabs extends Command
{
    /**
     * @var string
     */
    protected $signature = 'bpanel4-products:add-missing-tabs';

    /**
     * @var string
     */
    protected $description = '';

    public function handle(AdminMenu $adminMenu): void
    {
        Tabs::createItem(
            'bpanel4-products.create',
            'bpanel4-products.index',
            'bpanel4-products.index',
            'Listado',
            'fa fa-list'
        );
        Tabs::createItem(
            'bpanel4-products.edit',
            'bpanel4-products.index',
            'bpanel4-products.index',
            'Listado',
            'fa fa-list'
        );
        Tabs::createItem(
            'bpanel4-products.edit',
            'bpanel4-products.create',
            'bpanel4-products.create',
            'Nuevo',
            'fa fa-plus'
        );
    }

    private function createMenuEntries(AdminMenu $adminMenu): void
    {
        $this->comment('Añadiendo al menú de administración...');

        $adminMenu->createGroup('ecommerce', 'Tienda', 'far fa-shopping-cart');
        $adminMenu->createModule(
            'ecommerce',
            'bpanel4-products',
            'Productos',
            'far fa-box-full'
        );
        $adminMenu->createAction(
            'bpanel4-products',
            'Listado',
            'index',
            'fas fa-list'
        );
        $adminMenu->createAction(
            'bpanel4-products',
            'Nuevo',
            'create',
            'fas fa-plus'
        );
    }

    private function registerProductTrait(): void
    {
        if (App::environment('testing')) {
            return;
        }

        $traitsPath = base_path() . '/app/Traits';
        $file = $traitsPath . '/Bpanel4ProductTraits.php';

        if (!is_dir($traitsPath)) {
            mkdir($traitsPath);
        }

        touch($file);

        file_put_contents($file, "<?php\n\nnamespace App\Traits;\n\ntrait Bpanel4ProductTraits\n{\n\n}");

        chmod($file, 0644);

        $reflector = new \ReflectionClass(Product::class);
        $path = $reflector->getFileName();

        $originalContent = file_get_contents($path);

        if (\Illuminate\Support\Str::contains($originalContent, Bpanel4ProductTraits::class)) {
            return;
        }

        $modifiedContent = str_replace(
            'use HasTranslations;',
            "use HasTranslations;\n    use \\" . Bpanel4ProductTraits::class . ";",
            $originalContent
        );

        file_put_contents($path, $modifiedContent);
    }
}
