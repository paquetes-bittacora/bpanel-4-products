<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Commands;

use Bittacora\Category\Models\CategoryModel;
use Bittacora\Content\ContentFacade;
use Illuminate\Console\Command;

/*
 * Este comando se creó porque el instalador de bittacora/bpanel4-products creó inicialmente las categorías sin
 * asociarles contenidos, y provocaba fallos. No es necesario usarlo, lo mantengo por si viene bien más adelante.
 */
final class FixCategories extends Command
{
    /**
     * @var string
     */
    protected $signature = 'bpanel4-products:fix-categories';

    /**
     * @var string
     */
    protected $description = 'Corrige las categorías que se han creado sin asociarles un content.';

    public function handle(): void
    {
        $this->comment('Asociando content a las categorías que no lo tienen.');

        $categories = CategoryModel::all();

        foreach ($categories as $category) {
            if (null === $category->content) {
                ContentFacade::associateWithModel($category);
            }
        }

        $this->comment('Hecho');
    }
}
