<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Commands;

use App\Traits\Bpanel4ProductTraits;
use Artisan;
use Bittacora\AdminMenu\AdminMenu;
use Bittacora\AdminMenu\AdminMenuFacade;
use Bittacora\Bpanel4\Products\Bpanel4ProductsServiceProvider;
use Bittacora\Bpanel4\Products\Models\Product;
use Bittacora\Category\CategoryFacade;
use Bittacora\Tabs\Tabs;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class InstallCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'bpanel4-products:install';

    /**
     * @var string
     */
    protected $description = 'Instala el paquete para gestionar los productos de Bpanel4.';
    /**
     * @var string[]
     */
    private const PERMISSIONS = [
        'index',
        'create',
        'show',
        'edit',
        'destroy',
        'store',
        'update',
    ];

    public function handle(AdminMenu $adminMenu): void
    {
        $this->comment('Instalando el módulo Bpanel4 Products...');

        $this->createMenuEntries($adminMenu);
        $this->createTabs();

        $this->giveAdminPermissions();
        AdminMenuFacade::createAction('bpanel4-products', 'Categorías', 'category', 'fa fa-sitemap');

        CategoryFacade::addPermission('bpanel4-products');
        CategoryFacade::createTabs('bpanel4-products', 'Productos');

        Artisan::call('vendor:publish --tag=product-public-assets --force');
        Artisan::call("vendor:publish", [
            "--provider" => Bpanel4ProductsServiceProvider::class,
            "--tag" => "config"
        ]);


        $this->registerProductTrait();
    }

    private function giveAdminPermissions(): void
    {
        $this->comment('Añadiendo permisos...');
        $adminRole = Role::findOrCreate('admin');
        foreach (self::PERMISSIONS as $permission) {
            $permission = Permission::firstOrCreate(['name' => 'bpanel4-products.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }
    }

    private function createTabs(): void
    {
        Tabs::createItem(
            'bpanel4-products.index',
            'bpanel4-products.index',
            'bpanel4-products.index',
            'Listado',
            'fa fa-list'
        );
        Tabs::createItem(
            'bpanel4-products.index',
            'bpanel4-products.create',
            'bpanel4-products.create',
            'Nuevo',
            'fa fa-plus'
        );
    }

    private function createMenuEntries(AdminMenu $adminMenu): void
    {
        $this->comment('Añadiendo al menú de administración...');

        $adminMenu->createGroup('ecommerce', 'Tienda', 'far fa-shopping-cart');
        $adminMenu->createModule(
            'ecommerce',
            'bpanel4-products',
            'Productos',
            'far fa-box-full'
        );
        $adminMenu->createAction(
            'bpanel4-products',
            'Listado',
            'index',
            'fas fa-list'
        );
        $adminMenu->createAction(
            'bpanel4-products',
            'Nuevo',
            'create',
            'fas fa-plus'
        );
    }

    private function registerProductTrait(): void
    {
        if (App::environment('testing')) {
            return;
        }

        $traitsPath = base_path() . '/app/Traits';
        $file = $traitsPath . '/Bpanel4ProductTraits.php';

        if (!is_dir($traitsPath)) {
            mkdir($traitsPath);
        }

        touch($file);

        file_put_contents($file, "<?php\n\nnamespace App\Traits;\n\ntrait Bpanel4ProductTraits\n{\n\n}");

        chmod($file, 0644);

        $reflector = new \ReflectionClass(Product::class);
        $path = $reflector->getFileName();

        $originalContent = file_get_contents($path);

        if (\Illuminate\Support\Str::contains($originalContent, Bpanel4ProductTraits::class)) {
            return;
        }

        $modifiedContent = str_replace(
            'use HasTranslations;',
            "use HasTranslations;\n    use \\" . Bpanel4ProductTraits::class . ";",
            $originalContent
        );

        file_put_contents($path, $modifiedContent);
    }
}
