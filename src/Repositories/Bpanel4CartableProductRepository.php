<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Repositories;

use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Orders\Contracts\CartableProductRepository;
use Bittacora\Bpanel4\Products\Models\CartProduct;

final class Bpanel4CartableProductRepository implements CartableProductRepository
{
    public function getByProductId(int $productId): CartableProduct
    {
        return CartProduct::where('id', '=', $productId)->firstOrFail();
    }
}
