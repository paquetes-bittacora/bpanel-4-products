# Productos

Para publicar la configuración de este paquete, ejecutar el comando:

```
php artisan vendor:publish --tag=config --provider='Bittacora\Bpanel4\Products\Bpanel4ProductsServiceProvider'
```

# Shortcodes

Se incluye el shortcode `[featured-products]` que muestra un listado de productos destacados.

Tiene los siguientes parámetros:

- `numberofproducts`: Número de productos a mostrar
- `view`: Vista para renderizar la lista. Por defecto se usa `bpanel4-products::shortcodes.featured-products`

# 🔗 URLs

Las URLs que se generarán para los productos se pueden configurar con los parámetros
`product_route_prefix` y `product_url_structure`. La configuración por defecto es la siguiente:

```
'product_route_prefix' => '/productos',
'product_url_structure' => '/{product}/ver',
```

Esta configuración daría URLs como `/productos/producto-de-ejemplo/ver`, pero si, por ejemplo, se usa esta configuración:

```
'product_route_prefix' => '/producto',
'product_url_structure' => '/{product}',
```

Las URL resultantes serían del tipo `/productos/producto-de-ejemplo`. Esto puede ser útil de cara a 
respetar las URLs de la web anterior del cliente.
