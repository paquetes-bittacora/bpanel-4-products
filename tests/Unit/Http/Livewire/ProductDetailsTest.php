<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Tests\Unit\Http\Livewire;

use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4\Products\Http\Livewire\ProductDetails;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire;
use Mockery;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Tests\TestCase;

final class ProductDetailsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        rename(base_path('resources/views/vendor'), base_path('resources/views/__vendor__'));
    }

    public function testSePuedenReemplazarLaDescripcionCortaDelProducto(): void
    {
        $product = (new ProductFactory())->createOne();
        $component = Livewire::test(ProductDetails::class, ['productId' => $product->getId()]);

        // Este método es un listener en realidad, pero en el test lo disparo así
        $component->call('replaceProductDetails', ['short_description' => 'Nueva descripción corta']);

        $component->assertSee('Nueva descripción corta');
    }

    public function testSePuedenReemplazarLaDescripcionCompletaDelProducto(): void
    {
        $product = (new ProductFactory())->createOne();
        $component = Livewire::test(ProductDetails::class, ['productId' => $product->getId()]);

        $component->call('replaceProductDetails', ['description' => 'Nueva descripción completa']);

        $component->assertSee('Nueva descripción completa');
    }


    public function testSePuedenReemplazarLaImagenDestacadaDelProducto(): void
    {
        $product = (new ProductFactory())->createOne();
        $component = Livewire::test(ProductDetails::class, ['productId' => $product->getId()]);

        $component->call('replaceProductDetails', [
            'featured_image_url' => 'https://url.delaimagen.com/',
        ]);

        $component->assertSee('https://url.delaimagen.com');
    }

    public function testSePuedenReemplazarLasImagenesDelProducto(): void
    {
        $product = (new ProductFactory())->createOne();
        $component = Livewire::test(ProductDetails::class, ['productId' => $product->getId()]);

        $component->call('replaceProductDetails', [
            'featured_image_url' => 'https://url.delaimagen.com/', // Necesario aunque esté comprobando las otras
            'images' => [
                $this->getImageMock('https://url.delaimagen_2.com/'),
                $this->getImageMock('https://url.delaimagen_3.com/'),
            ],
        ]);

        $component->assertSee('https://url.delaimagen_2.com/');
        $component->assertSee('https://url.delaimagen_3.com/');
    }

    private function getImageMock(string $imageUrl): Media
    {
        $mock = Mockery::mock(Media::class);
        $mock->shouldReceive('getUrl')->andReturn($imageUrl);
        $mock->shouldReceive('getFullUrl')->andReturn($imageUrl);
        $mock->shouldIgnoreMissing();
        return $mock;
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        rename(base_path('resources/views/__vendor__'), base_path('resources/views/vendor'));
    }
}
