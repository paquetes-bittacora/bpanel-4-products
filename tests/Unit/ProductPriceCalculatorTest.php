<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Tests\Unit;

use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Clients\Services\ClientService;
use Bittacora\Bpanel4\Products\Models\Product;
use Bittacora\Bpanel4\Products\Services\ProductPriceCalculator;
use Mockery;
use PHPUnit\Framework\TestCase;

final class ProductPriceCalculatorTest extends TestCase
{
    private const PRICE = 50;
    private const DISCOUNTED_PRICE = 40;
    private Mockery\LegacyMockInterface|ClientService $clientServiceMock;
    private Mockery\LegacyMockInterface|Product $productMock;
    private ProductPriceCalculator $productPriceCalculator;

    public function setUp(): void
    {
        parent::setUp();

        $this->clientServiceMock = $this->getClientServicemock();
        $this->productMock = $this->getProductMock();
        $this->productMock->shouldIgnoreMissing();

        $this->productPriceCalculator = Mockery::mock(ProductPriceCalculator::class, [
            $this->productMock,
            $this->clientServiceMock,
        ])->makePartial();

        $this->productPriceCalculator->shouldReceive('getClientTypeName')->andReturn('any-user');
    }

    public function testCalculaElPrecioDelProducto(): void
    {
        self::assertEquals(
            self::PRICE,
            $this->productPriceCalculator->getPrice()->toFloat()
        );
    }

    public function testCalculaElPrecioRebajadoDelProducto(): void
    {
        $this->productMock->shouldReceive('getDiscountedPrice')
            ->andReturn(new Price(self::DISCOUNTED_PRICE));
        self::assertEquals(
            self::DISCOUNTED_PRICE,
            $this->productPriceCalculator->getDiscountedPrice()?->toFloat()
        );
    }

    private function getClientServicemock(): ClientService|Mockery\LegacyMockInterface
    {
        $clientServicemock = Mockery::mock(ClientService::class);
        $clientServicemock->shouldReceive('getClientTypeName')
            ->andReturn('any-user')->byDefault();
        return $clientServicemock;
    }

    private function getProductMock(): Product
    {
        $productMock = Mockery::mock(Product::class);
        $productMock->shouldReceive('getPrice')
            ->andReturn(new Price(self::PRICE));
        return $productMock;
    }

    public function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }
}
