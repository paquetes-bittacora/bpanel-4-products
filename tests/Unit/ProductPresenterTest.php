<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Tests\Unit;

use Bittacora\Bpanel4\Vat\Services\VatService;
use Bittacora\Bpanel4\Products\Models\Product;
use Bittacora\Bpanel4\Products\Presenters\ProductPresenter;
use Bittacora\Bpanel4\Products\Services\ProductPriceCalculator;
use Illuminate\Support\Collection;
use Mockery;
use Mockery\LegacyMockInterface;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;

final class ProductPresenterTest extends TestCase
{
    use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

    private LegacyMockInterface|ProductPriceCalculator $priceCalculatorMock;
    private LegacyMockInterface|Product $productMock;
    private ProductPresenter&MockInterface $productPresenter;

    protected function setUp(): void
    {
        parent::setUp();
        /** @var Product|LegacyMockInterface $productMock */
        $productMock = Mockery::mock(Product::class);
        $productMock->shouldReceive('getName')->andReturn('Producto de prueba');
        $productMock->shouldReceive('getSize')->andReturn(50);
        $productMock->shouldReceive('getUnit')->andReturn('ml');
        $this->productMock = $productMock;

        $this->priceCalculatorMock = Mockery::mock(ProductPriceCalculator::class);
        $this->priceCalculatorMock->shouldIgnoreMissing();
        $this->productPresenter = Mockery::mock(ProductPresenter::class, [
            $this->priceCalculatorMock,
            $productMock,
            new VatService(),
            1,
        ])->makePartial();
    }

    public function testDelegaEnPriceCalculatorElCalculoDeLosPrecios(): void
    {
        $this->priceCalculatorMock->shouldReceive('getPrice')->once();
        $this->priceCalculatorMock->shouldReceive('getDiscountedPrice')->once();
        $this->priceCalculatorMock->shouldReceive('getReference')->once();
        $this->productPresenter->getPrice();
        $this->productPresenter->getDiscountedPrice();
        $this->productPresenter->getReference();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }
}
