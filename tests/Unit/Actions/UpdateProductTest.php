<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Tests\Unit\Actions;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Products\Actions\UpdateProduct;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4\Products\Exceptions\DiscountedPriceConstraintException;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use PHPUnit\Framework\Attributes\CoversClass;
use Tests\TestCase;

#[CoversClass(UpdateProduct::class)]
final class UpdateProductTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test para comprobar que no se reproduce un error por el cual no se podía cambiar el precio de un producto de la
     * siguiente forma:
     * Paso 1: Precio normal -> 100 precio rebajado -> 99
     * Paso 2: Precio normal -> 80  precio rebajado -> 79
     * Paso 3: Precio normal -> 100 precio rebajado -> 99
     *              ==> Producía Bittacora\Bpanel4\Products\Exceptions\DiscountedPriceConstraintException
     * @throws InvalidPriceException
     */
    public function testSePuedeCambiarElPrecioDeUnProductoCuandoElNuevoPrecioSinDescuentoEsMayorQueElAnteriorPrecioConDescuento(): void
    {
        $product = (new ProductFactory())->createOne();
        $this->changeProductPrices($product, 100, 99);
        $this->changeProductPrices($product, 80, 79);

        $this->changeProductPrices($product, 100, 99);
    }

    /**
     * @throws InvalidPriceException
     */
    public function testNoSePuedePonerUnPrecioRebajadoSuperiorAlNormal(): void
    {
        $this->expectException(DiscountedPriceConstraintException::class);
        $product = (new ProductFactory())->createOne();

        $product->setPrice(new Price(50));
        $product->setDiscountedPrice(new Price(100));
        $product->save();
    }

    /**
     * @throws InvalidPriceException
     */
    private function changeProductPrices(Product $product, int $regularPrice, int $discountedPrice): void
    {
        $product->setPrice(new Price($regularPrice));
        $product->setDiscountedPrice(new Price($discountedPrice));
        $product->save();
        $product->refresh();
        $this->assertEquals((float) $regularPrice, $product->getPrice()->toFloat());
        $this->assertEquals((float) $discountedPrice, $product->getDiscountedPrice()?->toFloat());
    }
}