<?php

namespace Bittacora\Bpanel4\Products\Tests\Feature;

use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Bittacora\Content\Database\Factories\ContentModelFactory;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4\Products\Models\CartProduct;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use Tests\TestCase;

class CartProductTest extends TestCase
{
    use RefreshDatabase;

    public function testUtilizaElProductPresenterParaCalcularPrecios(): void
    {
        $cartProduct = $this->getProduct();
        self::assertInstanceOf(Price::class, $cartProduct->getUnitPrice());
    }

    public function testDevuelveLosDetallesDelProducto(): void
    {
        $cartProduct = $this->getProduct();
        self::assertIsString($cartProduct->getName());
        self::assertIsString($cartProduct->getReference());
    }

    public function testDevuelveElTipoDeIva(): void
    {
        $cartProduct = $this->getProduct();
        self::assertInstanceOf(VatRate::class, $cartProduct->getVatRate());
    }

    public function testDevuelveElPrecioConDescuento(): void
    {
        /** @var Product $product */
        $product = (new ProductFactory())->createOne();
        $product->setPrice(new Price(2));
        $product->setDiscountedPrice(new Price(1));
        $product->save();
        /** @var CartProduct $cartProduct */
        $cartProduct = $this->app->make(CartProduct::class);
        $cartProduct->setProductId($product->getId());

        self::assertEquals(1, $cartProduct->getUnitPrice()->toFloat());
    }

    public function testDevuelveUnaColeccionVaciaSiNoTieneImagenes(): void
    {
        $carProduct = $this->getProduct();
        self::assertInstanceOf(Collection::class, $carProduct->getImages());
    }

    /**
     * @return CartProduct
     */
    private function getProduct(): CartProduct
    {
        $product = (new ProductFactory())->has((new ContentModelFactory()), 'content')->createOne();
        /** @var CartProduct $cartProduct */
        $cartProduct = $this->app->make(CartProduct::class);
        $cartProduct->setProductId($product->getId());
        return $cartProduct;
    }
}
