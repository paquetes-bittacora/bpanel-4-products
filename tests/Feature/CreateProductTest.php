<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Tests\Feature;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Bittacora\Bpanel4\Vat\Tests\Feature\ObjectMothers\VatRateObjectMother;
use Bittacora\Bpanel4\Products\Actions\CreateProduct;
use Bittacora\Bpanel4\Products\Dtos\ProductCreationDto;
use Bittacora\Bpanel4\Products\Exceptions\DiscountedPriceConstraintException;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Database\Connection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Validation\ValidationException;
use Mockery;
use Mockery\Mock;
use Spatie\Permission\Models\Role;
use Tests\TestCase;
use Throwable;

final class CreateProductTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    private Connection|Mock $connectionMock;
    private CreateProduct $createProduct;
    private VatRateObjectMother $vatRateObjectMother;

    protected function setUp(): void
    {
        parent::setUp();
        $this->connectionMock = Mockery::mock(Connection::class)->shouldIgnoreMissing();
        $this->createProduct = $this->app->make(CreateProduct::class, ['db' => $this->connectionMock]);
        $this->vatRateObjectMother = new VatRateObjectMother();

        $vatRate = new VatRate();
        $vatRate->setName('IVA');
        $vatRate->setRate(21);
        $vatRate->setActive(true);
        $vatRate->save();
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testSePuedeCrearUnProductoSinTamanos(): void
    {
        $this->connectionMock->shouldReceive('commit')->once();
        $reference = $this->faker->slug(4);
        $this->createProduct->execute(new ProductCreationDto(
            reference: $reference,
            name: $this->faker->name(),
            vatRateId: $this->vatRateObjectMother->createVatRate()->getId(),
            price: Price::fromInt($this->faker->numberBetween(10, 100)),
            stock: $this->faker->numberBetween(0, 999),
            shortDescription: $this->faker->text(),
            description: $this->faker->text(1000),
            size: $this->faker->randomFloat(2, 100, 1000),
            unit: 'ml',
            active: true,
        ));

        $product = Product::where(['reference' => $reference])->firstOrFail();

        /** @phpstan-ignore-next-line Siempre es true pero quiero dejar el assert */
        self::assertInstanceOf(Product::class, $product);
        /** @phpstan-ignore-next-line Siempre es true pero quiero dejar el assert */
        self::assertIsInt($product->content->id);
    }

    /**
     * @throws Throwable
     */
    public function testNoSePuedeCrearUnPrecioConPrecioNegativo(): void
    {
        $this->expectException(InvalidPriceException::class);

        $this->createProduct->execute(new ProductCreationDto(
            reference: $this->faker->slug(3),
            name: $this->faker->name(),
            vatRateId: 1,
            price: Price::fromInt(-$this->faker->numberBetween(10, 100)),
        ));
    }

    /**
     * @throws Throwable
     */
    public function testElPrecioRebajadoNoPuedeSerIgualAlNormal(): void
    {
        $this->expectException(DiscountedPriceConstraintException::class);

        $price = $this->faker->numberBetween(10, 100);
        $this->createProduct->execute(new ProductCreationDto(
            reference: $this->faker->slug(3),
            name: $this->faker->name(),
            vatRateId: 1,
            price: Price::fromInt($price),
            discountedPrice: Price::fromInt($price),
        ));
    }

    /**
     * @throws Throwable
     */
    public function testElPrecioRebajadoNoPuedeSerMayorQueElNormal(): void
    {
        $this->expectException(DiscountedPriceConstraintException::class);

        $price = $this->faker->numberBetween(10, 100);
        $this->createProduct->execute(new ProductCreationDto(
            reference: $this->faker->slug(3),
            name: $this->faker->name(),
            vatRateId: 1,
            price: Price::fromInt($price),
            discountedPrice: Price::fromInt($price + 1),
        ));
    }

    /**
     * @throws InvalidPriceException
     */
    private function getProductCreationDto(string $reference): ProductCreationDto
    {
        return new ProductCreationDto(
            reference: $reference,
            name: $this->faker->name(),
            vatRateId: $this->vatRateObjectMother->createVatRate()->getId(),
            price: Price::fromInt($this->faker->numberBetween(0, 1000)),
        );
    }
}
