<?php

declare(strict_types=1);

namespace Feature;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Products\Actions\CreateProduct;
use Bittacora\Bpanel4\Products\Actions\UpdateProduct;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4\Products\Dtos\ProductCreationDto;
use Bittacora\Bpanel4\Products\Dtos\ProductUpdateDto;
use Bittacora\Bpanel4\Products\Models\Product;
use Bittacora\Bpanel4\Vat\Tests\Feature\ObjectMothers\VatRateObjectMother;
use Bittacora\Bpanel4Users\Tests\Helpers\AdminHelper;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Validation\ValidationException;
use Random\RandomException;
use Tests\TestCase;
use Throwable;

final class ProductPositionTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    private readonly AdminHelper $adminHelper;
    private readonly CreateProduct $createProduct;
    private readonly UpdateProduct $updateProduct;
    private readonly UrlGenerator $urlGenerator;
    private readonly VatRateObjectMother $vatRateObjectMother;

    protected function setUp(): void
    {
        parent::setUp();
        $this->adminHelper = $this->app->make(AdminHelper::class);
        $this->urlGenerator = $this->app->make(UrlGenerator::class);
        $this->createProduct = $this->app->make(CreateProduct::class);
        $this->updateProduct = $this->app->make(UpdateProduct::class);
        $this->vatRateObjectMother = $this->app->make(VatRateObjectMother::class);
    }

    public function testSeMuestraElCampoDeOrden(): void
    {
        // Arrange
        $this->adminHelper->actingAsAdminWithPermissions(['bpanel4-products.create']);

        // Act
        $result = $this->get($this->urlGenerator->route('bpanel4-products.create'));

        // Assert
        $result->assertSee('input type="number" name="position"', false);
    }

    /**
     * @throws Throwable
     * @throws RandomException
     * @throws InvalidPriceException
     * @throws ValidationException
     */
    public function testSePuedeCrearUnProductoConOrden(): void
    {
        // Arrange
        $position = random_int(-9999, 9999);
        $reference = $this->faker->randomAscii();

        // Act
        $this->createProduct->execute(new ProductCreationDto(
            reference: $reference,
            name: $this->faker->name(),
            vatRateId: $this->vatRateObjectMother->createVatRate()->getId(),
            price: Price::fromInt($this->faker->numberBetween(10, 100)),
            stock: $this->faker->numberBetween(0, 999),
            shortDescription: $this->faker->text(),
            description: $this->faker->text(1000),
            size: $this->faker->randomFloat(2, 100, 1000),
            unit: 'ml',
            active: true,
            position: $position,
        ));

        // Assert
        $product = Product::where('position', $position)->where('reference', $reference)->first();
        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals($position, $product->getPosition());
    }

    /**
     * @throws Throwable
     * @throws RandomException
     * @throws InvalidPriceException
     * @throws ValidationException
     */
    public function testSePuedeActualizarUnProductoConOrden(): void
    {
        // Arrange
        $product = (new ProductFactory())->createOne();
        $newPosition = random_int(-9999, 9999);

        // Act
        $this->updateProduct->execute($product, new ProductUpdateDto(
            id: $product->getId(),
            reference: $product->getReference(),
            name: $product->getName(),
            vatRateId: $product->getVatRate()->id,
            price: $product->getPrice(),
            discountedPrice: $product->getDiscountedPrice(),
            stock: 10,
            shortDescription: '',
            description: '',
            size: 5,
            unit: 'l',
            position: $newPosition,
        ));

        // Assert
        $updatedProduct = Product::where('position', $newPosition)->where('id', $product->getId())->first();
        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals($newPosition, $product->getPosition());
    }
}