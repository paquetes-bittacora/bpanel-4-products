<?php

/** @noinspection PhpRedundantVariableDocTypeInspection */

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Tests\Feature;

use Bittacora\Content\Database\Factories\ContentModelFactory;
use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4\Products\Http\Livewire\ProductsList;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Livewire\Livewire;
use Livewire\Testing\TestableLivewire;
use Tests\TestCase;

final class ProductListTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Config::set('bpanel4-products.show_prices_with_taxes', false);
    }

    public function testConstruyeElComponente(): void
    {
        $component = $this->getComponent();
        $component->assertOk();
    }

    /**
     * @dataProvider minPriceFilterDataProvider
     */
    public function testFiltraPorPrecioMinimoParaes(int $minPrice, int $count): void
    {
        $this->actingAs((new ClientFactory())->createOne()->getUser());
        $this->createProductsWithPrice();

        $component = $this->getComponent();
        $component->call('filtersUpdated', ['minPrice' => $minPrice]);

        self::assertEquals($count, $component->get('totalResults'));
    }

    /**
     * @dataProvider maxPriceFilterDataProvider
     */
    public function testFiltraPorPrecioMaximoParaes(int $maxPrice, int $count): void
    {
        $this->actingAs((new ClientFactory())->createOne()->getUser());
        $this->createProductsWithPrice();

        $component = $this->getComponent();
        $component->call('filtersUpdated', ['maxPrice' => $maxPrice]);

        self::assertEquals($count, $component->get('totalResults'));
    }


    /**
     * @dataProvider discountedMaxPriceFilterDataProvider
     */
    public function testTieneEnCuentaLosPreciosConDescuentoParaFiltrarPorPrecioMaximo(int $maxPrice, int $count): void
    {
        $this->createProduct(100, discountedPrice: 50);
        $component = $this->getComponent();
        $component->call('filtersUpdated', ['maxPrice' => $maxPrice]);

        self::assertEquals($count, $component->get('totalResults'));
    }

    /**
     * @dataProvider discountedMinPriceFilterDataProvider
     */
    public function testTieneEnCuentaLosPreciosConDescuentoParaFiltrarPorPrecioMinimo(int $minPrice, int $count): void
    {
        $this->createProduct(100, discountedPrice: 50);
        $component = $this->getComponent();
        $component->call('filtersUpdated', ['minPrice' => $minPrice]);

        self::assertEquals($count, $component->get('totalResults'));
    }

    public function testSeOrdenanLosProductos(): void
    {
        // Arrange
        $product1 = (new ProductFactory())->withPosition(3)->createOne();
        $product2 = (new ProductFactory())->withPosition(1)->createOne();
        $product3 = (new ProductFactory())->withPosition(2)->createOne();

        // Act
        $component = $this->getComponent();
        $component->set('filtersHaveBeenSet', true);

        // Assert
        $component->assertSeeTextInOrder([
            $product2->getName(),
            $product3->getName(),
            $product1->getName(),
        ]);
    }

    public function testElBuscadorNoEsSensibleALasMayusculas(): void
    {
        // Arrange
        $product1 = (new ProductFactory())->createOne();
        $product2 = (new ProductFactory())->createOne();
        $product3 = (new ProductFactory())->createOne();
        $product1->setName('NOMBRE DEL PRODUCTO 123');
        $product1->save();
        $product2->setName('nombre del producto 456');
        $product2->save();

        // Act
        $component = $this->getComponent();
        $component->call('filtersUpdated', ['name' => 'NOMBRE DEL PRODUCTO']);

        // Assert
        $component->assertSeeText('NOMBRE DEL PRODUCTO 123');
        $component->assertSeeText('nombre del producto 456');
        $component->assertDontSee($product3->getName());
    }

    /**
     * @return array<int, array<int>>
     */
    public static function minPriceFilterDataProvider(): array
    {
        return [
            [0, 4],
            [10, 4],
            [11, 3],
            [20, 3],
            [21, 2],
        ];
    }

    /**
     * @return array<int, array<int>>
     */
    public static function maxPriceFilterDataProvider(): array
    {
        return [
            [1, 0],
            [10, 1],
            [11, 1],
            [60, 4],
        ];
    }


    /**
     * @return array<int, array<int>>
     */
    public static function discountedMaxPriceFilterDataProvider(): array
    {
        return [
            [49, 0],
            [50, 1],
        ];
    }

    /**
     * @return array<int, array<int>>
     */
    public static function discountedMinPriceFilterDataProvider(): array
    {
        return [
            [51, 0],
            [1, 1],
        ];
    }

    private function createProduct(
        int $price,
        ?int $discountedPrice = null,
    ): void {
        (new ProductFactory())->has((new ContentModelFactory()), 'content')
            ->withPrice($price)
            ->withDiscountedPrice($discountedPrice)
            ->createOne();
    }

    private function createProductsWithPrice(): void
    {
        $this->createProduct(10);
        $this->createProduct(20);
        $this->createProduct(40);
        $this->createProduct(60);
    }

    private function getComponent(): TestableLivewire
    {
        return Livewire::test(ProductsList::class);
    }
}
