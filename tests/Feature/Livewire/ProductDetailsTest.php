<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Tests\Feature\Livewire;

use Bittacora\Bpanel4\Orders\Actions\Cart\AddProductToCart;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Bittacora\Bpanel4\Vat\Services\VatService;
use Bittacora\Content\Database\Factories\ContentModelFactory;
use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4\Products\Http\Livewire\ProductDetails;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Livewire\Livewire;
use Mockery\MockInterface;
use Tests\TestCase;

final class ProductDetailsTest extends TestCase
{
    use RefreshDatabase;

    private Product $product;
    private VatService $vatService;

    public function setUp(): void
    {
        parent::setUp();
        $this->product = $this->getProduct();
        $this->withoutExceptionHandling();

        $client = (new ClientFactory())->createOne();
        $this->actingAs($client->getUser());
        $this->vatService = $this->app->make(VatService::class);
        Config::set('bpanel4-products.show_prices_with_taxes', false);
    }

    public function testMuestraElNombreDelProducto(): void
    {
        Livewire::test(ProductDetails::class, ['productId' => $this->product->getId()])
            ->assertSee($this->product->getName());
    }

    public function testMuestraElPrecioOriginalYElRebajadoSiLoHay(): void
    {
        Config::set('bpanel4-products.show_prices_with_taxes', false);
        /** @var Product $product */
        $product = (new ProductFactory())->has((new ContentModelFactory()), 'content')
            ->has((new ContentModelFactory()), 'content')
            ->withDiscount()->createOne();

        Livewire::test(ProductDetails::class, ['productId' => $product->getId()])
            ->assertSee($product->getDiscountedPrice(), $product->getVatRate());
    }

    public function testAnadeLaCantidadSeleccionadaAlProducto(): void
    {
        $this->mock(AddProductToCart::class, function (MockInterface $mock): void {
            /** @phpstan-ignore-next-line  */
            $mock->shouldReceive('execute')->withArgs(fn (...$args): bool => 3 === (int)$args[2]);
        });

        $component = Livewire::test(ProductDetails::class, ['productId' => $this->product->getId()]);
        $component->set('quantity', 3);
        $component->call('addToCart', $this->product->getId(), null);

        /** @phpstan-ignore-next-line  */
        $this->assertTrue(true); // Si no hago algo así, marca el test como risky
    }

    private function getProduct(): Product
    {
        $product =  (new ProductFactory())->has((new ContentModelFactory()), 'content')->createOne();
        return $product;
    }

    private function removeTaxes(Price $price, VatRate $vatRate): Price
    {
        return Price::fromInt($this->vatService->discountVatFromPrice($price->toInt(), $vatRate));
    }
}
