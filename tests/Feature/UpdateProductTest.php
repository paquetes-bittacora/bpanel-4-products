<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Tests\Feature;

use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Tests\Feature\ObjectMothers\VatRateObjectMother;
use Bittacora\Bpanel4\Products\Actions\UpdateProduct;
use Bittacora\Bpanel4\Products\Dtos\ProductUpdateDto;
use Bittacora\Bpanel4\Products\Models\Product;
use Bittacora\Bpanel4\Products\Tests\Feature\ObjectMothers\ProductObjectMother;
use Illuminate\Database\Connection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Validation\ValidationException;
use Mockery;
use Tests\TestCase;
use Throwable;

final class UpdateProductTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    private Mockery\Mock|Connection $connectionMock;
    private ProductObjectMother $productObjectMother;
    private UpdateProduct $updateProduct;
    private VatRateObjectMother $vatRateObjectMother;

    public function setUp(): void
    {
        parent::setUp();

        $this->connectionMock = Mockery::mock(Connection::class)->shouldIgnoreMissing();
        $this->updateProduct = $this->app->make(UpdateProduct::class, ['db' => $this->connectionMock]);
        $this->productObjectMother = $this->app->make(ProductObjectMother::class);
        $this->vatRateObjectMother = new VatRateObjectMother();
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testSePuedeModificarUnProducto(): void
    {
        $product = $this->productObjectMother->random();
        $this->connectionMock->shouldReceive('commit')->once();

        $newName = $this->faker->name();
        $newReference = $this->faker->slug(4);
        $newPrice = Price::fromInt($this->faker->numberBetween(5, 10));
        $discountedPrice = Price::fromInt($this->faker->numberBetween(1, 4));
        $vatRateId = $this->vatRateObjectMother->createVatRate()->getId();
        $this->updateProduct->execute($product, new ProductUpdateDto(
            id: $product->getId(),
            reference: $newReference,
            name: $newName,
            vatRateId: $vatRateId,
            price: $newPrice,
            discountedPrice: $discountedPrice,
            stock: 10,
            shortDescription: '',
            description: '',
            size: 5,
            unit: 'l',
        ));

        $product = Product::where(['reference' => $newReference])->firstOrFail();
        self::assertEquals($newName, $product->getName());
        self::assertEquals($newReference, $product->getReference());
        self::assertEqualsWithDelta($newPrice, $product->getPrice(), 0.0002);
        self::assertEquals('l', $product->getUnit());
        self::assertEquals(10, $product->getStock());
        self::assertEquals($vatRateId, $product->getVatRate()->getId());
        self::assertEquals(5, $product->getSize());
        self::assertEqualsWithDelta($discountedPrice, $product->getDiscountedPrice(), 0.0002);
    }

}
