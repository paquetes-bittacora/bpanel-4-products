<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Tests\Feature\ObjectMothers;

use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Bittacora\Bpanel4\Vat\Tests\Feature\ObjectMothers\VatRateObjectMother;
use Bittacora\Bpanel4\Products\Actions\CreateProduct;
use Bittacora\Bpanel4\Products\Dtos\ProductCreationDto;
use Bittacora\Bpanel4\Products\Models\Product;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Validation\ValidationException;
use Throwable;

final class ProductObjectMother
{
    private readonly Generator $faker;
    private readonly VatRate $vatRate;

    public function __construct(
        private readonly CreateProduct $createProduct,
        private readonly VatRateObjectMother $vatRateObjectMother,
    ) {
        $this->faker = Factory::create('es_ES');
        $this->vatRate = $this->vatRateObjectMother->createVatRate();
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function random(): Product
    {
        $reference = $this->faker->slug(4);
        $this->createProduct->execute(new ProductCreationDto(
            reference: $reference,
            name: $this->faker->name(),
            vatRateId: $this->vatRate->getId(),
            price: Price::fromInt($this->faker->numberBetween(10, 100)),
            stock: $this->faker->numberBetween(0, 999),
            shortDescription: $this->faker->text(),
            description: $this->faker->text(1000),
            size: $this->faker->randomFloat(2, 100, 1000),
            unit: 'ml',
            active: true,
        ));

        return Product::where(['reference' => $reference])->firstOrFail();
    }
}
