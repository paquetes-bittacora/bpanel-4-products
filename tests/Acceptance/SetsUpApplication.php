<?php

namespace Bittacora\Bpanel4\Products\Tests\Acceptance;

use Bittacora\AdminMenu\Database\Seeders\AdminMenuSeeder;
use Bittacora\Bpanel4Users\Database\Factories\UserFactory;
use Illuminate\Contracts\Routing\UrlGenerator;
use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\LivewireCountryStateSelector\Models\State;
use Bittacora\LivewireCountryStateSelector\Database\Factories\CountryFactory;
use Bittacora\LivewireCountryStateSelector\Database\Factories\StateFactory;
use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\Vat\Database\Factories\VatRateFactory;

trait SetsUpApplication
{
    /**
     * @return void
     */
    protected function setUpApplication(): void
    {
        $this->urlGenerator = $this->app->make(UrlGenerator::class);
        (new UserFactory())->createOne();
        $this->artisan("db:seed", ['class' => AdminMenuSeeder::class]);
        $this->createCountryAndState();
        $this->artisan('bpanel4-products:install');
        $this->artisan('bpanel4-clients:install');
        $this->withoutExceptionHandling();

        $client = (new ClientFactory())->createOne();
        $user = $client->getUser();
        $user->givePermissionTo('bpanel4-products.store');
        $user->givePermissionTo('bpanel4-products.update');
        $user->givePermissionTo('bpanel4-products.index');
        $this->actingAs($user);
    }

    /**
     * @return void
     */
    private function createCountryAndState(): void
    {
        $country = (new CountryFactory())->createOne();
        $country->name = 'España';
        $country->save();

        $state = (new StateFactory())->createOne();
        $state->name = 'Badajoz';
        $state->save();
    }
}
