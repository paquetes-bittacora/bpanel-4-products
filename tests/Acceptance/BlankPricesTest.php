<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Tests\Acceptance;

use Bittacora\Bpanel4\Vat\Database\Factories\VatRateFactory;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * Hay un error con los precios cuando se dejan en blanco y creo este test para que no se repita.
 */
final class BlankPricesTest extends TestCase
{
    use RefreshDatabase;
    use SetsUpApplication;

    private UrlGenerator $urlGenerator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setUpApplication();
    }

    public function testSePuedeCrearUnProductoConPrecioRebajadoEnBlanco(): void
    {
        $reference = bin2hex(random_bytes(8));
        $response = $this->followingRedirects()->post(
            $this->urlGenerator->route('bpanel4-products.store'),
            [
                'name' => 'Prueba',
                'reference' => $reference,
                'price' => 10,
                'discounted_price' => '',
                'vat_rate_id' => (new VatRateFactory())->createOne()->getId(),
                'stock' => 99,
                'short_description' => 'Descripción corta',
                'description' => 'Descripción',
                'size' => '100',
                'unit' => 'ml',
                'active' => true,
                'position' => 0
            ]
        );
        $response->assertOk();
        $response->assertSee('El producto se creó correctamente');

        $product = Product::whereReference($reference)->firstOrFail();
        self::assertNull($product->getDiscountedPrice());
    }

    public function testSePuedeEditarUnProductoConPrecioRebajadoEnBlanco(): void
    {
        $product = (new ProductFactory())->createOne();
        $reference = bin2hex(random_bytes(8));
        $response = $this->followingRedirects()->post(
            $this->urlGenerator->route(
                'bpanel4-products.update',
                ['product' => $product]
            ),
            [
                'id' => $product->getId(),
                'name' => 'Prueba',
                'reference' => $reference,
                'price' => 10,
                'discounted_price' => '',
                'vat_rate_id' => (new VatRateFactory())->createOne()->getId(),
                'stock' => 99,
                'short_description' => 'Descripción corta',
                'description' => 'Descripción',
                'size' => '100',
                'unit' => 'ml',
                'active' => true,
                'locale' => 'es',
                'position' => 0,
            ]
        );
        $response->assertOk();
        $response->assertSee('El producto se actualizó correctamente');

        $product = Product::whereReference($reference)->firstOrFail();
        self::assertNull($product->getDiscountedPrice());
    }
}
