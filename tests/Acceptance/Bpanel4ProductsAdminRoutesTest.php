<?php

namespace Bittacora\Bpanel4\Products\Tests\Acceptance;

use Bittacora\Bpanel4Users\Database\Factories\UserFactory;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

final class Bpanel4ProductsAdminRoutesTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    use CreatesRequestData;

    public function setUp(): void
    {
        parent::setUp();
        Role::create(['name' => 'any-user']);
        $this->actingAs((new UserFactory())->withPermissions(
            'bpanel4-products.index',
            'bpanel4-products.create',
            'bpanel4-products.store',
            'bpanel4-products.edit',
            'bpanel4-products.update',
        )->createOne());

        $this->withoutExceptionHandling();
    }

    public function testCargaLaVistaDeCreacionDeProducto(): void
    {
        $this->get(route('bpanel4-products.create'))->assertOk();
    }

    public function testPuedeCrearUnProducto(): void
    {
        $response = $this->followingRedirects()->post(
            route('bpanel4-products.store'),
            $this->getRequestData(),
        );
        $response->assertSessionHasNoErrors();
        $response->assertSee(__('bpanel4-products::product.created'));
    }

    public function testCargaLaVistaDeEdicionDeProducto(): void
    {
        $product = (new ProductFactory())->createOne();
        $id = $product->getId();
        $response = $this->get(
            route('bpanel4-products.edit', ['model' => $id]),
            $this->getRequestData() + ['id' => $id],
        );

        $response->assertOk();
    }

    public function testSePuedeActualizarUnProducto(): void
    {
        $product = (new ProductFactory())->createOne();
        $id = $product->getId();
        $response = $this->followingRedirects()->post(
            route('bpanel4-products.update', ['product' => $id]),
            $this->getRequestData() + ['id' => $id],
        );
        $response->assertSessionHasNoErrors();
        $response->assertSee(__('bpanel4-products::product.updated'));
    }
}
