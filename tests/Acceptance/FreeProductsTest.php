<?php

namespace Bittacora\Bpanel4\Products\Tests\Acceptance;

use Bittacora\Bpanel4\Vat\Database\Factories\VatRateFactory;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

final class FreeProductsTest extends TestCase
{
    use RefreshDatabase;
    use SetsUpApplication;
    private UrlGenerator $urlGenerator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setUpApplication();
    }

    public function testPuedeCrearseUnProductoGratis(): void
    {
        $reference = $this->getReference();
        $requestData = $this->getRequestData($reference);
        $response = $this->postCreationRequest($requestData);
        $this->assertProductWasCreated($response);

        /** @var Product $product */
        $product = Product::whereReference($reference)->firstOrFail();
        self::assertEquals(0, $product->getPrice()->toInt());
    }

    public function testNoSePuedeCrearUnProductoConPrecioesVacio(): void
    {
        self::expectException(ValidationException::class);
        self::expectExceptionMessage('El campo price debe ser un número');

        $reference = $this->getReference();
        $requestData = $this->getRequestData($reference);
        $requestData['price'] = '';
        $this->postCreationRequest($requestData);
    }

    public function testPuedeEditarseUnProductoGratis(): void
    {
        $product = (new ProductFactory())->createOne();
        $reference = $this->getReference();
        $requestData = $this->getRequestData($reference);
        $requestData['id'] = $product->getId();
        $response = $this->postUpdateRequest($requestData);
        $this->assertProductWasUpdated($response);

        /** @var Product $product */
        $product = Product::whereReference($reference)->firstOrFail();
        self::assertEquals(0, $product->getPrice()->toInt());
    }

    public function testNoSePuedeEditarUnProductoConPrecioesVacio(): void
    {
        $product = (new ProductFactory())->createOne();
        self::expectException(ValidationException::class);
        self::expectExceptionMessage('El campo price debe ser un número');

        $reference = $this->getReference();
        $requestData = $this->getRequestData($reference);
        $requestData['price'] = '';
        $requestData['id'] = $product->getId();
        $this->postUpdateRequest($requestData);
    }

    /**
     * @return array<string, mixed>
     */
    private function getRequestData(string $reference): array
    {
        return [
            'name' => 'Prueba',
            'reference' => $reference,
            'price' => 0,
            'discounted_price' => '',
            'vat_rate_id' => (new VatRateFactory())->createOne()->getId(),
            'stock' => 99,
            'short_description' => 'Descripción corta',
            'description' => 'Descripción',
            'size' => '100',
            'unit' => 'ml',
            'active' => true,
            'locale' => 'es',
            'position' => 0,
        ];
    }

    /**
     * @throws \Exception
     */
    private function getReference(): string
    {
        return bin2hex(random_bytes(8));
    }

    private function postCreationRequest(array $requestData): \Illuminate\Testing\TestResponse
    {
        return $this->followingRedirects()->post(
            $this->urlGenerator->route('bpanel4-products.store'),
            $requestData
        );
    }

    private function postUpdateRequest(array $requestData): \Illuminate\Testing\TestResponse
    {
        return $this->followingRedirects()->post(
            $this->urlGenerator->route('bpanel4-products.update', ['product' => $requestData['id']]),
            $requestData
        );
    }

    private function assertProductWasCreated(\Illuminate\Testing\TestResponse $response): void
    {
        $response->assertOk();
        $response->assertSee('El producto se creó correctamente');
    }

    private function assertProductWasUpdated(\Illuminate\Testing\TestResponse $response): void
    {
        $response->assertOk();
        $response->assertSee('El producto se actualizó correctamente');
    }
}
