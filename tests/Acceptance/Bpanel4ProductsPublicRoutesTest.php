<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Tests\Acceptance;

use Bittacora\Content\Database\Factories\ContentModelFactory;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Tests\TestCase;

final class Bpanel4ProductsPublicRoutesTest extends TestCase
{
    use RefreshDatabase;
    use SetsUpApplication;

    protected function setUp(): void
    {
        parent::setUp();
        $this->createCountryAndState();
        Config::set('bpanel4-products.show_prices_with_taxes', false);
    }

    public function testElCatalogoDeProductosCarga(): void
    {
        (new ProductFactory())->count(5)->has((new ContentModelFactory()), 'content')->create();
        $response = $this->get(route('bpanel4-products-public.index'));
        $response->assertOk();
    }

    public function testMuestraFichaPublicaDeProducto(): void
    {
        /** @var Product $product */
        $product = (new ProductFactory())->count(5)->has((new ContentModelFactory()), 'content')->createOne();
        $response = $this->get(route('bpanel4-products-public.show', ['product' => $product->getId()]));
        $response->assertOk();
        $response->assertViewIs('bpanel4-products::public.show');
    }

    public function testSeMuestraPrecioSinIvaSiShowPricesWithTaxesEsFalse(): void
    {
        Config::set('bpanel4-products.show_prices_with_taxes', false);
        /** @var Product $product */
        $product = (new ProductFactory())->count(5)->has((new ContentModelFactory()), 'content')->createOne();
        $response = $this->get(route('bpanel4-products-public.show', ['product' => $product->getId()]));
        $response->assertOk();
        $this->assertStringContainsString(
            (string) $product->getPrice()->toFloat(),
            $response->getContent(),
        );
    }

    public function testSeMuestraPrecioConIvaSiShowPricesWithTaxesEsTrue(): void
    {
        Config::set('bpanel4-products.show_prices_with_taxes', true);
        /** @var Product $product */
        $product = (new ProductFactory())->count(5)->has((new ContentModelFactory()), 'content')->createOne();
        $response = $this->get(route('bpanel4-products-public.show', ['product' => $product->getId()]));
        $response->assertOk();
        $this->assertStringContainsString(
            (string) ($product->getPrice()->toFloat() * 1.21),
            $response->getContent(),
        );
    }
}
