<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Tests\Acceptance;

use Bittacora\Bpanel4\Vat\Database\Factories\VatRateFactory;

trait CreatesRequestData
{
    /**
     * @return array<string, mixed>
     */
    protected function getRequestData(): array
    {
        return [
            'reference' => $this->faker->slug(),
            'name' => $this->faker->name(),
            'price' => $this->faker->numberBetween(10, 100),
            'discounted_price' => null,
            'vat_rate_id' => (new VatRateFactory())->createOne()->getId(),
            'stock' => 99,
            'short_description' => $this->faker->text(),
            'description' => $this->faker->text(),
            'size' => 100,
            'unit' => 'ml',
            'active' => true,
            'locale' => 'es',
            'position' => 0,
        ];
    }
}
