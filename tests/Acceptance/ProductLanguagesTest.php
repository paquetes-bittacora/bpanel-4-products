<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Tests\Acceptance;

use Bittacora\Bpanel4Users\Database\Factories\UserFactory;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class ProductLanguagesTest extends TestCase
{
    use RefreshDatabase;
    use CreatesRequestData;
    use WithFaker;

    private string $originalProductName;
    private Product $product;
    private UrlGenerator $urlGenerator;

    protected function setUp(): void
    {
        parent::setUp();

        $this->urlGenerator = $this->app->make(UrlGenerator::class);
        $this->product = (new ProductFactory())->createOne();
        $this->originalProductName = $this->product->getName();
        $this->actingAs((new UserFactory())->withPermissions(
            'bpanel4-products.edit',
            'bpanel4-products.update'
        )->createOne());
    }

    public function testCargaLaPaginaDeEdicionDeProductoConElIdiomaIndicado(): void
    {
        $response = $this->get($this->urlGenerator->route('bpanel4-products.edit', [
            'model' => $this->product,
            'locale' => 'pt',
        ]));

        $response->assertSee('value="pt"', false);
    }

    public function testAlEditarEnPortuguesNoSeModificaEnEspanol(): void
    {
        $requestData = $this->getRequestData();
        $requestData['locale'] = 'pt';
        $requestData['id'] = $this->product->getId();

        $this->post($this->urlGenerator->route('bpanel4-products.update', ['product' => $this->product]), $requestData);

        $product = Product::whereId($this->product->getId())->firstOrFail();

        $this->assertEquals($this->originalProductName, $product->getTranslation('name', 'es'));
        $this->assertEquals($requestData['name'], $product->getTranslation('name', 'pt'));
    }
}
