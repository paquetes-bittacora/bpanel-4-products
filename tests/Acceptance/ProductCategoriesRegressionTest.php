<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Tests\Acceptance;

use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\Products\Models\Product;
use Bittacora\Category\Models\CategoryModel;
use Bittacora\Content\ContentFacade;
use Bittacora\Language\Models\LanguageModel;
use Bittacora\Language\Models\Locale;
use Bittacora\Language\Models\Region;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

final class ProductCategoriesRegressionTest extends TestCase
{
    use RefreshDatabase;

    /*
     * Hago este test porque al ir a editar una categoría me ha saltado una excepción.
     */
    public function testCargaLaVistaDeEdicionDeUnaCategoria(): void
    {
        $this->prepareEnvironment();
        $response = $this->get('/bpanel/productos/product/category/1/edit/language');
        $response->assertOk();
    }

    private function prepareEnvironment(): void
    {
        $this->withoutExceptionHandling();
        Role::create(['name' => 'admin']);

        $locale = Locale::create(['code' => 'co', 'name' => 'ES']);
        $region = Region::create([
            'name' => 'name',
            'iso' => 'iso',
            'alfa_2' => 'alfa2',
            'alfa_3' => 'alfa3',
            'code' => 'co',
        ]);
        LanguageModel::create([
            'name' => 'ES',
            'default' => 1,
            'locale' => $locale->code,
            'region' => $region->alfa_2,
        ]);

        Artisan::call('bpanel4-products:install');
        $client = (new ClientFactory())->createOne();
        $user = $client->getUser();
        $user->givePermissionTo('bpanel4-products.editCategory');

        $this->actingAs($user);

        $category = CategoryModel::create([
            'title' => 'prueba',
            'model' => Product::class,
            'meta_title' => '',
            'meta_description' => '',
            'meta_keywords' => '',
            'default' => 1
        ]);
        ContentFacade::associateWithModel($category);
    }
}
