<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Database\Factories;

use Bittacora\Bpanel4\Products\Models\CartProduct;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<CartProduct>
 */
final class CartProductFactory extends Factory
{
    /**
     * @var class-string<\Bittacora\Bpanel4\Products\Models\CartProduct>
     */
    protected $model = CartProduct::class;

    /**
     * @return array<string, \Illuminate\Database\Eloquent\Model>|array<string, string>
     */
    public function definition(): array
    {
        return [
            'product_id' => (new ProductFactory())->createOne(),
        ];
    }

    public function withPrice(float $price): self
    {
        return $this->state(function (array $attributes) use ($price) {
            $product = (new ProductFactory())->withPrice($price)
                ->createOne();
            return [
                'product_id' => $product
            ];
        });
    }

    public function withDiscount(): self
    {
        return $this->state(function (): array {
            $product = (new ProductFactory())->withDiscount()
                ->createOne();
            return [
                'product_id' => $product,
            ];
        });
    }
}
