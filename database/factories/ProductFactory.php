<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Products\Database\Factories;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Database\Factories\VatRateFactory;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Collection;

/**
 * @extends Factory<Product>
 * @method Product createOne($attributes = [])
 * @method self count(?int $count)
 * @method Collection<int, Product> create()
 */
class ProductFactory extends Factory
{
    /**
     * @var class-string<Product>
     */
    protected $model = Product::class;

    /**
     * @throws InvalidPriceException
     */
    public function definition(): array
    {
        return [
            'reference' => $this->faker->asciify(),
            'name' => $this->faker->name(),
            'price' => new Price($this->faker->numberBetween(30, 50)),
            'discounted_price' => null,
            'description' => 'Descripcion del producto',
            'short_description' => 'Descripción corta del producto',
            'vat_rate_id' => (new VatRateFactory())->createOne(),
            'position' => 0,
            'stock' => 999,
            'meta_title' => $this->faker->title(),
            'meta_description' => $this->faker->text(),
            'meta_keywords' => $this->faker->words(3, true),
        ];
    }

    public function withPrice(float $price): ProductFactory
    {
        return $this->state(function () use ($price) {
            return ['price' => new Price($price)];
        });
    }

    public function withDiscountedPrice(?float $price): ProductFactory
    {
        if (null === $price) {
            return $this;
        }

        return $this->state(function () use ($price) {
            return ['discounted_price' => new Price($price)];
        });
    }

    public function withDiscount(): ProductFactory
    {
        return $this->state(function () {
            return [
                'discounted_price' => new Price($this->faker->numberBetween(10, 30)),
                'price' => new Price(999),
            ];
        });
    }

    public function withPosition(int $position): ProductFactory
    {
        return $this->state(function () use ($position) {
            return ['position' => $position];
        });
    }

}
