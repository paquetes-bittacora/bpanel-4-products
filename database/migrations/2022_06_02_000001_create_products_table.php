<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('products', static function (Blueprint $table) {
            $table->id();
            $table->string('reference', 255);
            $table->json('name', 255);
            $table->json('short_description', 1000)->nullable();
            $table->json('description')->nullable();
            $table->integer('price');
            $table->integer('discounted_price')->nullable();
            $table->float('size')->comment('Tamaño del producto sin la unidad (por ejemplo, volumen)')
                ->nullable();
            $table->string('unit')->comment('Unidad de medida (ml, kg...)')->nullable();
            $table->unsignedBigInteger('stock')->nullable();
            $table->unsignedBigInteger('order_column')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
