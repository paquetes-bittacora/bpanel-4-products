<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    const TABLES = 'products';

    public function up(): void
    {
        Schema::table(self::TABLES, static function (Blueprint $table) {
            // No uso SeoFacade para añadir los campos porque ya había añadido el slug
            $table->json('meta_title');
            $table->json('meta_description');
            $table->json('meta_keywords');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table(self::TABLES, static function (Blueprint $table) {
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_description');
            $table->dropColumn('meta_keywords');
        });
    }
};
