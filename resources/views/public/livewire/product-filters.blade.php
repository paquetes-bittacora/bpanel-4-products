<div class="catalog-filters">
    <div class="mb-2 text-dv-blue"><h2><i class="fas fa-filter"></i> FILTROS</h2></div>
    {{-- Nombre --}}
    <div class="filter">
        <input type="text" wire:model="name" wire:change="updateFilters" wire:keyup="updateFilters"
               placeholder="Introduzca al menos 3 caracteres...">
    </div>
    {{-- Categorías --}}
    @if (!empty($availableCategories))
        <div class="filter categories-filter">
            <div class="name">CATEGORÍAS</div>
            <ul>
                <li @if(!isset($categories) || empty($categories)) class="selected" @endif>
                    <a href="#" wire:click="clearCategoriesFilter()">Todas</a>
                </li>
                @foreach($availableCategories as $category)
                    @if(null === $category->parent_id)
                        <li @if (in_array($category->id, $categories))class="selected"@endif >
                            <a href="#" wire:click="toggleCategory({{ $category->id }})">{{ $category->title }}</a>
                            @if (!$category->children->isEmpty())
                                <ul class="subcategories">
                                    @foreach($category->children as $subcategory)
                                        <li><a href="#" wire:click="toggleCategory({{ $subcategory->id }})"><i class="fas fa-chevron-right"></i> {{ $subcategory->title }}</a></li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    @endif
    {{-- Precio --}}
    <div class="filter">
        <div class="name">PRECIO</div>
        <div class="price-filter">
            <div>
                <label for="minPrice">Mín.: </label>
                <input type="number" wire:model="minPrice" wire:change="updateFilters" min="0" id="minPrice"
                       max="{{ $maxPrice }}">
            </div>
            <div>
                <label for="maxPrice">Máx.:</label>
                <input type="number" wire:model="maxPrice" wire:change="updateFilters" id="maxPrice"
                       min="{{ $minPrice }}">
            </div>
        </div>
    </div>
    @stack('additional-filters')
</div>
