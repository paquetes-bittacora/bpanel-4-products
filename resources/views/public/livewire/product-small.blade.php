<?php /** @var \Bittacora\Bpanel4\Products\Presenters\ProductPresenter $productPresenter */ ?>
<div>
    <div class="product-small">
        <a href="{{ route('bpanel4-products-public.show', [
            'product' => !empty($productPresenter->getSlug()) ? $productPresenter->getSlug() : $productPresenter->getProductId()
        ]) }}">
            <div class="image">
                @if (!empty($productPresenter->getFeaturedImages()))
                    <img src="{{ $productPresenter->getFeaturedImages()[0]->getUrl('product-thumb') }}"
                         alt="{{ $productPresenter->getName() }}">
                @else
                    <img src="/assets/img/product-placeholder.png" alt="">
                @endif
            </div>
        </a>
        <div class="categories">
            @foreach($productPresenter->getCategories() as $category)
                <a href="{{ route('bpanel4-products-public.index') }}?categories[0]={{ $category->id }}">{{ $category->title }}</a>
            @endforeach
        </div>
        <div class="product-name">
            <a href="{{ route('bpanel4-products-public.show', ['product' => $productPresenter->getProductId()]) }}">
                {{ $productPresenter->getName() }}
            </a>
        </div>
        <div>
            <a class="add-to-cart" href="#"
               wire:click.prevent="addToCart({{ $productPresenter->getProductId() }})">
                <img src="/assets/img/add-to-cart.png"
                     alt="Añadir {{ $productPresenter->getName() }} al carrito">
            </a>
            @if(config('bpanel4-products.show_prices_with_taxes'))
                <span class="price">{{ $productPresenter->getPrice() }}</span>
            @else
                <span class="price">{{ $productPresenter->getPriceWithoutVat() }}<span
                            class="vat-notice">(iva no incl.)</span></span>
            @endif
        </div>
    </div>
</div>
