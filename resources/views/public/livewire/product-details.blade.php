<?php /**
 * @var \Bittacora\Bpanel4\Products\Models\Product $product
 * @var \Bittacora\Bpanel4\Products\Presenters\ProductPresenter $productPresenter
 */ ?>
<div>
    <div class="row mb-5">
        <div class="col-6">
            {{-- Galería --}}
            @if (!empty($productPresenter->getFeaturedImages()) || !empty($featuredImageUrl))
                <div class="img-container">
                    <img src="{{ $featuredImageUrl }}"
                         alt="{{ $productPresenter->getName() }}">
                </div>
                @if (!empty($productImages))
                    <div class="thumbnails-container">
                        @foreach($productImages as $image)
                            <div class="thumb-container" wire:click="setFeaturedImageUrl('{{ $image->getFullUrl() }}')">
                                <img src="{{ $image->getUrl('product-thumb') }}"
                                     alt="{{ $productPresenter->getName() }}">
                            </div>
                        @endforeach
                    </div>
                @endif
            @else
                <div class="img-container">
                    <img src="/assets/img/product-placeholder.png" alt="">
                </div>
            @endif
        </div>
        <div class="col-6">
            {{-- Detalles del producto --}}
            <h1 class="product-title">{{ $product->getName() }}</h1>
            <div>
                <div class="text-dv-blue">Unidades</div>
                <div><input type="number" step="1" min="1" wire:model="quantity"></div>
            </div>
            <div class="d-flex align-items-center">
                @if(config('bpanel4-products.show_prices_with_taxes'))
                    <span class="price">{{ $productPresenter->getPrice() }}</span>
                @else
                    <span class="price">{{ $productPresenter->getPriceWithoutVat() }}<span class="vat-notice">(iva no incl.)</span></span>
                @endif
                <a class="add-to-cart" href="#"
                   wire:click="addToCart({{ $productPresenter->getProductId() }})">
                    <button><img
                                src="{{ Vite::asset('packages/bpanel4-public-pages/resources/assets/img/add-to-cart-icon.svg') }}"
                                alt="">
                        Añadir al carrito
                    </button>
                </a>
            </div>
            <div class="short-description">
                {{ $product->getShortDescription() }}
            </div>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-12">
            <h2>Descripción</h2>
            {!! $product->getDescription() !!}
        </div>
    </div>
</div>
