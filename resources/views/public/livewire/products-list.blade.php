<div>
    <div @if(count($products) > 0) class="product-catalog-grid" @endif>
        @forelse($products as $product)
            @livewire('bpanel4-products::public.product-small', ['productId' => $product->getId()],
            key($product->id))
        @empty
            <p style="width: 100%; min-height: 60vh; display: flex; align-items: center; justify-content: center; font-weight: bold; font-size: 1.5rem;">
                @if($filtersHaveBeenSet)
                    No se encontró ningún producto
                @else
                    Cargando...
                @endif
            </p>
        @endforelse
    </div>
    @if(null === $limit)
        {{ $products->links() }}
    @endif
    <div wire:loading.flex class="loading">
        <i class="fas fa-spinner fa-spin"></i>
    </div>
    <script>
      document.addEventListener('readystatechange', function () {
        Livewire.emit('updateFilters');
      });
    </script>
    <script>
      document.addEventListener('DOMContentLoaded', function () {
        Livewire.on('productListUpdated', () => {
          document.querySelectorAll('.animate').forEach(function (element) {
            if (element) {
              element.classList.add('in-viewport');
            }
          });
        });
      });
    </script>
</div>
