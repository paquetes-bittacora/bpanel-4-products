<?php /** @var \Bittacora\Bpanel4\Products\Models\Product $product */ ?>

@section('meta-title') {{ $product->meta_title ?? $product->getName() }} @endsection
@section('meta-description'){{ $product->meta_description ?? strip_tags($product->getShortDescription()) }}@endsection
@section('meta-keywords'){{ $product->meta_keywords ?? str_replace(' ', ',', $product->getName()) }}@endsection

@extends('bpanel4-public.layouts.regular-page')

@section('title')
    {{ $product->getName() }}
@endsection

@section('content')
    <div class="regular-page-container product-details">
        @livewire('bpanel4-products::public.livewire.product-details', ['productId' => $product->getId()])
    </div>
@endsection
