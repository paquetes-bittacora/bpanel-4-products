<div class="catalog">
    <div class="product-filters">
        <div class="mb-2 text-dv-blue"><h2><i class="fas fa-filter"></i> FILTROS</h2></div>
        <div class="filter">
            <form method="get" action="/productos">
                <input type="text" name="name" placeholder="Introduzca al menos 3 caracteres...">
            </form>
        </div>
        {{-- Categorías --}}
        @if (!empty($availableCategories))
            <div class="filter categories-filter">
                <div class="name">CATEGORÍAS</div>
                <ul>
                    @foreach($availableCategories as $category)
                        @if(null === $category->parent_id)
                            <li>
                                <a href="/productos?categories[0]={{$category->id}}">{{ $category->title }}</a>
                                @if (!$category->children->isEmpty())
                                    <ul class="subcategories">
                                        @foreach($category->children as $subcategory)
                                            <li><a href="/productos?categories[0]={{$subcategory->id}}"><i class="fas fa-chevron-right"></i> {{ $subcategory->title }}</a></li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>
