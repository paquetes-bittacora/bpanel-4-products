@extends('bpanel4-public.layouts.regular-page')

@section('meta-title')
    {{ $selectedCategoryName ?? 'Listado de productos'}}
@endsection

@section('content')
    <div class="regular-page-container">
        <div class="breadcrumbs">{{ config('app.name') }} / <span class="selected-category-name">{{ $selectedCategoryName }}</span></div>
        <h1 class="page-title selected-category-name">{{ $selectedCategoryName }}</h1>
        <div class="catalog">
            <div class="product-filters">
                @livewire('bpanel4-products::public.livewire.product-filters')
            </div>
            <div class="products-list">
                @livewire('bpanel4-products::public.livewire.products-list')
            </div>
        </div>
    </div>
@endsection

<script>
  document.addEventListener('DOMContentLoaded', function () {
    Livewire.on('categoryNameUpdated', categoryName => {
      document.querySelectorAll('.selected-category-name').forEach(function (item) {
        item.innerText = categoryName;
        document.title = categoryName + ' | {{ config('app.name') }}';
      });
    })
  });
</script>
