<td class="align-middle">
    <div class="text-center">
        {{$row->name}}
    </div>
</td>
<td class="align-middle">
    <div class="text-center">
        {{$row->reference}}
    </div>
</td>
<td class="align-middle">
    <div class="text-center">
        {{$row->size}} {{$row->unit}}
    </div>
</td>
<td class="align-middle">
    <div class="text-center">
        {{ $row->getPrice() }}
    </div>
</td>
<td class="align-middle">
    @if(!empty($row->getDiscountedPrice()))
        <div class="text-center">
            {{ $row->getDiscountedPrice() }}
        </div>
    @endif
</td>
<td class="align-middle">
    <div class="text-center">
        {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at)->format('d/m/Y')}}
    </div>
</td>
<td class="align-middle">
    @livewire('utils::datatable-default', ['fieldName' => 'active', 'model' => $row, 'value' => $row->active, 'size' => 'xxs'], key('active-user-'.$row->id))
</td>
<td class="align-middle">
    @livewire('utils::datatable-default', ['fieldName' => 'featured', 'model' => $row, 'value' => $row->featured, 'size' => 'xxs'], key('featured-user-'.$row->id))
</td>
<td class="align-middle">
    <div class="text-center">
        @livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'bpanel4-products', 'model' => $row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'el producto?'], key('user-buttons-'.$row->id))
    </div>
</td>
