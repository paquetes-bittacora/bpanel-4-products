@extends('bpanel4::layouts.bpanel-app')

@section('title', __('bpanel4-products::form.new-product'))

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ $panelTitle }}</span>
            </h4>
            @if(isset($language))
                @include('language::partials.form-languages', ['model' => $product, 'edit_route_name' => 'bpanel4-products.edit',  'currentLanguage' => $language])
            @endif
        </div>
        <form class="mt-lg-3" autocomplete="off" method="post" action="{{ $action }}" enctype="multipart/form-data">
            @csrf
            @livewire('form::input-text', ['name' => 'name', 'labelText' => __('bpanel4-products::product.name'), 'required'=>true, 'value' => old('name') ?? $product?->getName() ])
            @livewire('form::input-text', ['name' => 'reference', 'labelText' => __('bpanel4-products::product.reference'), 'required'=>true, 'value' => old('reference') ?? $product?->getReference() ])
            @livewire('form::textarea', ['name' => 'short_description', 'labelText' => __('bpanel4-products::product.short_description'), 'value' => old('short_description') ?? $product?->getShortDescription() ])
            @livewire('utils::tinymce-editor', ['name' => 'description', 'labelText' => __('bpanel4-products::product.description'), 'value' => old('description') ?? $product?->getDescription() ])
            @stack('after-description-additional-fields')
            <x-bpanel4-vat::select :model="$product"/>
            @livewire('form::input-number', ['name' => 'price', 'labelText' => __('bpanel4-products::product.price_form_label'), 'required'=>true, 'value' => old('price') ?? $product?->getPrice()->toFloat(), 'min' => 0, 'step' => 0.0001, 'fieldWidth' => 7, 'labelWidth' => 3 ])
            <x-price-preview vatFieldId="vat_rate_id" priceFieldId="price"></x-price-preview>
            @livewire('form::input-number', ['name' => 'discounted_price', 'labelText' => __('bpanel4-products::product.discounted_price_form_label'), 'required'=>false, 'value' => old('discounted_price') ?? $product?->getDiscountedPrice()?->toFloat(), 'min' => 0, 'step' => 0.0001, 'fieldWidth' => 7, 'labelWidth' => 3, 'helpText' => __('bpanel4-products::form.discounted_price_help') ])
            <x-price-preview vatFieldId="vat_rate_id" priceFieldId="discounted_price"></x-price-preview>
            @livewire('form::input-number', ['name' => 'size', 'labelText' => __('bpanel4-products::product.size'), 'required'=>false, 'value' => old('size') ?? $product?->getSize(), 'step' => 0.01, 'min' => 0, 'fieldWidth' => 7, 'labelWidth' => 3, 'helpText' => __('bpanel4-products::form.size_help'), 'placeholder' => 'Por ejemplo, tamaño del envase: 100, 200, 500 (sin indicar la unidad de medida)' ])
            @livewire('form::input-text', ['name' => 'unit', 'labelText' => __('bpanel4-products::form.unit'), 'required'=>false, 'value' => old('unit') ?? $product?->getUnit(),
            'placeholder' => __('bpanel4-products::form.unit-placeholder') ])
            @livewire('form::input-number', ['name' => 'stock', 'labelText' => __('bpanel4-products::product.stock'), 'required'=>true, 'value' => old('stock') ?? $product?->getStock(), 'step' => 1, 'min' => 0, 'fieldWidth' => 7, 'labelWidth' => 3 ])
            <x-bpanel4-category::select :model="$product" :categories="$categories" :multiple="true"/>
            @if (null !== $product and null !== $product->content)
                <div class="row">
                    <div class="col-3">&nbsp;</div>
                    <div class="col-7">
                        @livewire('content-multimedia::upload-content-multimedia-widget', [
                        'fieldName' => 'product_images',
                        'model' => $product,
                        'allowedFormats' => 'image/jpeg, image/png, image/webp',
                        'allowedExtensions' => 'jpg,jpeg,png,webp'
                        ])
                        @livewire('content-multimedia-images::content-multimedia-images-widget', [
                        'contentId' => $product->content->id,
                        'module' => 'product',
                        'permission' => 'product.edit'
                        ])
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-3"></div>
                    <div class="col-7">
                        <x-bpanel4-warning message="Para añadir imágenes al producto, primero debe guardarlo."/>
                    </div>
                </div>
            @endif
            <div class="form-group form-row">
                <div class="col-sm-3 col-form-label text-sm-right pr-0"></div>
                <div class="col-sm-7">
                    @livewire('seo::seo-fields', ['model' => $product])
                </div>
            </div>
            @livewire('form::input-number', ['name' => 'position', 'labelText' => __('bpanel4-products::form.position'), 'required'=>true, 'value' => old('position') ?? $product?->getPosition() ?? 0, 'step' => 1, 'min' => -999999, 'max' => '999999', 'fieldWidth' => 7, 'labelWidth' => 3, 'helpText' => __('bpanel4-products::form.position_field_help') ])
            @stack('before-active-additional-fields')
            @livewire('form::input-checkbox', ['name' => 'active', 'value' => 1, 'checked' => old('active') ?? $product?->isActive(), 'labelText' => __('bpanel4-products::product.active'), 'bpanelForm' => true])
            @livewire('form::input-checkbox', ['name' => 'featured', 'value' => 1, 'checked' => old('featured') ?? $product?->isFeatured(), 'labelText' => __('bpanel4-products::product.featured'), 'bpanelForm' => true])
            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form::save-button',['theme'=>'update'])
                @livewire('form::save-button',['theme'=>'reset'])
            </div>
            @if($product?->getId() !== null)
                <input type="hidden" name="id" value="{{ $product->getId() }}">
            @endif

            @if(isset($language))
                <input type="hidden" name="locale" value="{{ $language }}">
            @endif
        </form>
    </div>

@endsection
