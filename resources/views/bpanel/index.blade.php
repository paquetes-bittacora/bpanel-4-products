@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Productos')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('bpanel4-products::datatable.index') }}</span>
            </h4>
        </div>
        <div class="card-body">
            @livewire('bpanel4-products::livewire.products-table', [], key('bpanel4-products-datatable'))
        </div>
    </div>

    @if ($lowStockProducts->count() > 0)
        <div class="alert alert-danger mt-5">
            <strong class="mb-3 d-block"><i class="fas fa-box"></i> Los siguientes productos tienen stock bajo:</strong>
        @foreach($lowStockProducts as $lowStockProduct)
            <div class="pb-2">
                <a href="{{ route('bpanel4-products.edit', ['model' => $lowStockProduct]) }}" target="_blank">{!! strip_tags($lowStockProduct->name) !!}</a> (Stock: {{ $lowStockProduct->stock }})
            </div>
        @endforeach
        </div>
    @endif

@endsection
