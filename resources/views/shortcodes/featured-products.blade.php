<div class="featured-products">
    @foreach($products as $product)
        <div class="product">
            @if (!empty($product->getFeaturedImages()))
                <div class="img-container">
                    <img src="{{ $product->getFeaturedImages()[0]->getFullUrl() }}"
                         alt="{{ $product->getName() }}">
                </div>
            @else
                <div class="img-container">
                    <img src="/assets/img/product-placeholder.png" alt="">
                </div>
            @endif
            <div class="name">{{ $product->getName() }}</div>
            <div class="price">{{ $product->getPrice() }}</div>
        </div>
    @endforeach
</div>
