<?php

declare(strict_types=1);

return [
    'skin-conditions' => 'Condiciones de la piel',
    'protocols' => 'Protocolos',
];
