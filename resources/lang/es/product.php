<?php

declare(strict_types=1);

return [
    'name' => 'Nombre',
    'reference' => 'Referencia',
    'short_description' => 'Descripción corta',
    'description' => 'Descripción',
    'price' => 'Precio',
    'discounted_price' => 'Precio rebajado',
    'price_form_label' => 'Precio (sin IVA)',
    'discounted_price_form_label' => 'Precio rebajado (sin IVA)',
    'size' => 'Tamaño',
    'stock' => 'Stock',
    'created' => 'El producto se creó correctamente',
    'updated' => 'El producto se actualizó correctamente',
    'active' => 'Activado',
    'featured' => 'Destacado',
    'category' => 'Categoría',
    'deleted' => 'El producto se eliminó correctamente',
    'price' => 'Precio',
    'discounted_price' => 'Precio rebajado',
];
