<?php

declare(strict_types=1);

return [
    'bpanel4-products' => 'Productos',
    'create' => 'Nuevo',
    'index' => 'Listado',
    'edit' => 'Editar',
    'editCategory' => 'Editar categoría',
    'createCategory' => 'Crear categoría',
    'category' => 'Categorías',
    'attributes' => 'Atributos',
];
