<?php

declare(strict_types=1);

return [
    'new-product' => 'Nuevo producto',
    'edit' => 'Editar',
    'unit-placeholder' => 'Unidad de medida en la que se indica el tamaño, por ejemplo ml, gr, etc.',
    'unit' => 'Unidad de medida',
    'discounted_price_help' => 'Déjelo en blanco si el producto no tiene precio rebajado',
    'size_help' => 'Si se deja en blanco no se mostrará en la ficha del producto',
    'edit-product' => 'Editar producto',
    'position' => 'Posición',
    'position_field_help' => 'Cuanto menor sea el número, más arriba aparecerá el producto en los listados. Se pueden usar números negativos',
];
