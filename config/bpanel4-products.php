<?php

declare(strict_types=1);

return [
    // Establece si se muestran los precios con IVA o sin IVA.
    'show_prices_with_taxes' => true,
    // Para poder añadir plugins al módulo, hago dinámica la clase que se usará para representar un producto
    'product_class' => \Bittacora\Bpanel4\Products\Models\Product::class,
    // Clase que se usará para el componente de la datatable del listado de productos en el panel
    'datatable_admin_component_class' => \Bittacora\Bpanel4\Products\Http\Livewire\ProductsDatatable::class,
    // Productos por página en el catálogo
    'products_per_page' => 17,
    // --------- IMÁGENES ---------
    // Nota: Si se cambian estos valores, habrá que ejecutar php artisan media-library:regenerate
    // Ancho de la miniatura del producto
    'product_thumbnail_width' => 400,
    // Alto de la miniatura del producto
    'product_thumbnail_height' => 400,
    // --------- URLs ---------
    // Prefijo para la ruta de los productos en la parte pública, originalmente "/productos"
    'product_route_prefix' => '/productos',
    // Estructura de la ruta para los productos, originalmente "/{product}/ver"
    'product_url_structure' => '/{product}/ver',
];