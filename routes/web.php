<?php

declare(strict_types=1);

use Bittacora\Category\CategoryFacade;
use Bittacora\Bpanel4\Products\Http\Controllers\ProductAdminController;
use Bittacora\Bpanel4\Products\Http\Controllers\ProductPublicController;
use Bittacora\Bpanel4\Products\Models\Product;

Route::prefix('/productos')->name('bpanel4-products-public.')->middleware(['web'])->group(function () {
    Route::get('/', [ProductPublicController::class, 'index'])->name('index');
});

Route::prefix(config('bpanel4-products.product_route_prefix'))->name('bpanel4-products-public.')->middleware(['web'])->group(function () {
    Route::get(config('bpanel4-products.product_url_structure'), [ProductPublicController::class, 'show'])->name('show');
});


Route::prefix('bpanel/productos')->name('bpanel4-products.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(function () {
        Route::get('/', [ProductAdminController::class, 'index'])->name('index');
        Route::get('/nuevo', [ProductAdminController::class, 'create'])->name('create');
        Route::get('{model}/editar/{locale?}', [ProductAdminController::class, 'edit'])->name('edit');
        Route::post('/store', [ProductAdminController::class, 'store'])->name('store');
        Route::post('{product}/actualizar', [ProductAdminController::class, 'update'])->name('update');
        Route::delete('{product}/destroy', [ProductAdminController::class, 'destroy'])->name('destroy');

        CategoryFacade::addRoutes('product', Product::class, ProductAdminController::class);
    });

Route::prefix('bpanel/etiquetas/productos')->name('bpanel4-products.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(function () {
        Route::get('/condiciones-piel')->name('skin-conditions');
        Route::get('/protocolos')->name('protocols');
    });
